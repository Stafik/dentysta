public with sharing class AppointmentReservation {

    public Id dentist { get; set; }


    public List<SelectOption> getListOfDentists() {

        List<SelectOption> dentists = new List<SelectOption>();

        //List<Dentist__c> = [SELECT FirstName__c, LastName__c FROM Dentist__c];
        for (Dentist__c dent : [SELECT FirstName__c, LastName__c FROM Dentist__c]) {
            dentists.add(new SelectOption(dent.Id, dent.FirstName__c + ' ' + dent.LastName__c));
        }
        return dentists;
    }

    public AppointmentDatesWrapper.ReturnedDates[] getAvailableAppointmentDatesForTheWeek() {
        // AppointmentDatesWrapper
        AppointmentDatesWrapper appDatesWrapper = new AppointmentDatesWrapper(DateTime.now(), dentist);
        System.debug('appDatesWrapper: ' + appDatesWrapper.availableWorkingHours);
        
        return appDatesWrapper.availableWorkingHours;
    }
}