public with sharing class AppointmentsListController {
	
    public Appointment__c appmnt { get; set; }
    public List<Appointment__c> listOfAppmnts { get; set; }
    public String clientName { get; set; }
    public String appmntType { get; set; }
    public String appmntStatus { get; set; }
    
    public AppointmentsListController() {
    	
    	appmnt = new Appointment__c();
    }
    
    public PageReference displayTable() {
    	
    	clientName = '%' + appmnt.Name + '%';
    	appmntType = appmnt.AppointmentType__c;
    	appmntStatus = appmnt.AppointmentStatus__c;
    	
    	String query = buildQuery(appmnt.Name, appmnt.AppointmentType__c, appmnt.AppointmentStatus__c);
    	
    	listOfAppmnts = Database.query(query);
    	System.debug('listOfAppmnts: ' + listOfAppmnts);
    	System.debug('clientName: ' + clientName + '; type: ' + appmntType + '; status: ' + appmntStatus);
    	
    	return null;
    }
    
    private String buildQuery(String appName, String appType, String appStatus) {
    	
    	String result = 'SELECT Name, AppointmentType__c, AppointmentStatus__c, StartDateAndTime__c, EndDateAndTime__c, Contact__r.Id, Contact__r.Name ' +
    					'FROM Appointment__c WHERE Id != \'\'';
    	
    	if (appName != null) {
    		result += ' AND Contact__r.Name LIKE \'%' + appName + '%\'';
    	}
    	if (appType != null) {
    		result += ' AND AppointmentType__c = \'' + appType + '\'';
    	}
    	if (appStatus != null) {
    		result += ' AND AppointmentStatus__c = \'' + appStatus +'\'';
    	}
    	
    	System.debug('build Query result: ' + result);
    	return result;
    }
}