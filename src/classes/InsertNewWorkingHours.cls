global class InsertNewWorkingHours implements Database.Batchable<sObject> {

	global final String query;
	
	public InsertNewWorkingHours() {
		
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {

		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope) {

	}

	global void finish(Database.BatchableContext BC) {

	}
}