public with sharing class UpdateWorkingHoursHelper {

	public static Map<String, Integer> dayToInteger = new Map<String, Integer> {
                'Poniedziałek' => 1, 'Wtorek' => 2, 'Środa' => 3, 'Czwartek' => 4,
                'Piątek' => 5, 'Sobota' => 6, 'Niedziela' => 7 };

	public static Integer calculateNumericDayOfTheWeek(DateTime dateInQuestion) {

        // Gets the day of the week using non-well documented Java 'format' method. 
        // It doesn't depend on locale (e.g. Sunday is the first day of the week in US, contrary to Europe).
        // format('u') is available only for DateTime class.
        // 1 => MON, 2 => TUE, 4 => WED, ..., 7 => SUN
        Integer result = Integer.valueOf(dateInQuestion.format('u'));

    	return result;
	}

	public static Date firstWeekDayThisMonth(String dayOfTheWeekName) {

		Date result;
		Integer dayOfTheWeek, dayOfTheWeekToday, difference;

		dayOfTheWeek = dayToInteger.get(dayOfTheWeekName);
		dayOfTheWeekToday = calculateNumericDayOfTheWeek(DateTime.now());

			// calculate the difference between today and the day in question
		difference = dayOfTheWeek < dayOfTheWeekToday ? 7 - dayOfTheWeekToday + dayOfTheWeek : dayOfTheWeekToday - dayOfTheWeek;

		result = DateTime.now().date().addDays(difference);

		return result;
	}

	public static Date lastWeekDayOfTheMonthGiven(String dayOfTheWeekName, Integer howManyMonthsAhead) {

		DateTime tempDate;
		Integer startingDayOfTheWeek = dayToInteger.get(dayOfTheWeekName);
		Date result;

		tempDate = DateTime.now();
		tempDate = tempDate.addMonths(howManyMonthsAhead + 1);
		tempDate = tempDate.date().toStartOfMonth();
		tempDate = tempDate - 1;	// let's get the last day of the previous month

		while (startingDayOfTheWeek != calculateNumericDayOfTheWeek(tempDate)) {
			tempDate = tempDate - 1;
		}

		result = tempDate.date();

		return result;
	}
}

//	public static DateTime firstWeekDayThisMonth(DateTime dateValue) {

//		DateTime result;
//		Integer dayOfTheWeek, dayOfTheWeekToday, difference;

//		dayOfTheWeek = calculateNumericDayOfTheWeek(dateValue);
//		dayOfTheWeekToday = calculateNumericDayOfTheWeek(DateTime.now());

//			// calculate the difference between today and the day in question
//		difference = dayOfTheWeek < dayOfTheWeekToday ? 7 - dayOfTheWeekToday + dayOfTheWeek : dayOfTheWeekToday - dayOfTheWeek;

//		result = dateValue.addDays(difference);

//		return result;
//	}

//	public static DateTime lastWeekDayOfTheMonthGiven(DateTime startDate, Integer howManyMonthsAhead) {

//		DateTime result, tempDate;
//		Integer startingDayOfTheWeek = calculateNumericDayOfTheWeek(startDate);
//		Date d;
//		Time t;

//		tempDate = startDate;
//		tempDate = tempDate.addMonths(howManyMonthsAhead + 1);
//		tempDate = tempDate.date().toStartOfMonth();
//		tempDate = tempDate - 1;	// let's get the last day of the previous month

//		while (startingDayOfTheWeek != calculateNumericDayOfTheWeek(tempDate)) {
//			tempDate = tempDate - 1;
//		}

//		d = tempDate.date();
//		t = startDate.time();

//		result = DateTime.newInstance(d, t);

//		return tempDate;
//	}
//}