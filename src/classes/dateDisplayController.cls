public class dateDisplayController {
    
    public Integer timeValue { get; set; }
    public Integer hours, minutes;
    
    public void setHours(Integer h) {
        hours = h;
    }
    
    public Integer getHours() {
        Integer result = Double.valueOf(timeValue/60).intValue();
        return result;
    }
    
    public void setMinutes(Integer m) {
        minutes = m;
    }
    
    public Integer getMinutes() {
        Integer minutes = Math.mod(timeValue, 60);
        return minutes;
    }
}