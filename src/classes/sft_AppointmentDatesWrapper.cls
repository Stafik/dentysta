public class sft_AppointmentDatesWrapper implements Comparable {
    
	public Integer dayOfWeek { get; set; }
	public InnerDate[] listOfInnerDates { get; set; }

    
    public sft_AppointmentDatesWrapper() {
        listOfInnerDates = new List<InnerDate>();
    }

	public class InnerDate {
			// input radio value
		public Boolean slotPickedToSave;
		public Boolean getSlotPickedToSave() { 
			if(slotPickedToSave == null) 
				slotPickedToSave = false; 
			return slotPickedToSave; 
		}
		public void setSlotPickedToSave (Boolean val) {
			this.slotPickedToSave = val;
		}
        public Datetime startDateTime { get; set; } 
		public Datetime stopDateTime { get; set; }
	}
    
    public void addInnerDate(Datetime start, Datetime stop) {
        InnerDate id = new InnerDate();
        System.debug(start + ' - ' + stop);
        id.startDateTime = start;
        id.stopDateTime = stop;
        
        listOfInnerDates.add(id);
    }
    
    public void addInnerDate(Date tdate, Time tstart, Time tstop) {
        InnerDate id = new InnerDate();
        Datetime start, stop;
        start = Datetime.newInstance(tdate, tstart);
        stop = Datetime.newInstance(tdate, tstop);
        
        id.startDateTime = start;
        id.stopDateTime = stop;
        
        listOfInnerDates.add(id);
        System.debug('adding: ' + start + ' - ' + stop);
    }
    
    	// fetches the Date related to the particular object
    public Date getFirstDate(){
    	return listOfInnerDates[0].startDateTime.date();
    }
    
    	// replaces the dates (used to add working hours exceptions)
    public void replaceInnerDates(InnerDate[] datesToReplace) {
    	
    	this.listOfInnerDates = datesToReplace;
    }
    
    public void makeWorkingSlots(Integer slotDuration) {
    	
    		// let's copy the listOfInnerDates and clear it
    	InnerDate[] listOfInnerDatesCopy = this.listOfInnerDates.clone();
    	Date tdate = listOfInnerDates[0].startDateTime.date();
    	Time tstart;
    	
    	this.listOfInnerDates.clear();
    	
    	for (InnerDate idate : listOfInnerDatesCopy) {
    			// for (startTime of the current workingHours period; until startTime+slotDuration doesn't exceed the stopTime; add slotDuration and start over
    		for (tstart = idate.startDateTime.time(); tstart.addMinutes(slotDuration) <= idate.stopDateTime.time(); tstart = tstart.addMinutes(slotDuration)) {
    			this.addInnerDate(tdate, tstart, tstart.addMinutes(slotDuration));
    			//System.debug('tdate = ' + tdate + ', tstart = ' + tstart + ', tstart+slotDuration = ' + tstart.addMinutes(slotDuration));
    		}
    	}
    }

    public void removeAlreadyBookedSlots(InnerDate[] slotListToRemove) {
        
        for(InnerDate idate : slotListToRemove) {
        	for(Integer i = listOfInnerDates.size() - 1; i >= 0; i--) {
        		if((idate.startDateTime >= listOfInnerDates[i].startDateTime && idate.startDateTime < listOfInnerDates[i].stopDateTime) ||
                    (idate.stopDateTime > listOfInnerDates[i].startDateTime && idate.stopDateTime <= listOfInnerDates[i].stopDateTime)) {
                        System.debug('Inner Date to remove: ' + listOfInnerDates[i]);
                        listOfInnerDates.remove(i);
                	}
        	}
        }
    }
    
    public Integer compareTo(Object compareTo) {
        
        sft_AppointmentDatesWrapper compareToADW = (sft_AppointmentDatesWrapper)compareTo;
        Date thisDate, anotherDate;
        thisDate = compareToADW.listOfInnerDates.get(0).startDateTime.date();
        anotherDate = listOfInnerDates.get(0).startDateTime.date();
        
        if (thisDate == anotherDate)
            return 0;
        if (thisDate > anotherDate)
            return 1;
        return -1;
    }
    
}