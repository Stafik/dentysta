@isTest
class sft_AppointmentDatesWrapperTest {
    
    static testmethod void slotPickedToSaveTest() {
    	
    	sft_AppointmentDatesWrapper adw = new sft_AppointmentDatesWrapper();
    	adw.addInnerDate(Datetime.now(), Datetime.now().addMinutes(30));
    
    	Boolean result = adw.listOfInnerDates[0].getSlotPickedToSave();
    	System.assertEquals(result, false);
    	
    	adw.listOfInnerDates[0].setSlotPickedToSave(true);
    	System.assertEquals(adw.listOfInnerDates[0].slotPickedToSave, true);
    }
 
}