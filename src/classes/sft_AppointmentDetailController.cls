public with sharing class sft_AppointmentDetailController {
    
    public Appointment__c[] appmnts { get; set; }
    private String appmntId;
    
    
    public sft_AppointmentDetailController() {
    	
    	appmntId = ApexPages.currentPage().getParameters().get('id');
    	
    	if (appmntId != null) {
    		try {
    		appmnts = [SELECT Id, AppointmentType__c, AppointmentStatus__c, StartDateAndTime__c, EndDateAndTime__c, Contact__r.Name, Description__c
    		          FROM Appointment__c
    		          WHERE Id = :appmntId];
    		}
    		catch (DmlException e) {
    			System.debug('Can\'t fetch the Appointment\'s details in sft_AppointmentDetail page: ' + e.getMessage());
    		}
    	}
    	System.debug('appmnt: ' + appmnts);
    }
    
    
    public PageReference goToAppointmentsList() {
    	
    	return Page.sft_AppointmentsList;
    }
}