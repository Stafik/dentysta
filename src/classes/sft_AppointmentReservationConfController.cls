public class sft_AppointmentReservationConfController {
    
	public Id appmntBookedId { get; set; }
	public Appointment__c appmnt { get; set; }
	
	public sft_AppointmentReservationConfController() {
		appmntBookedId = ApexPages.currentPage().getParameters().get('id');
		System.debug('id = ' + appmntBookedId);
		if (appmntBookedId != null) {
			Appointment__c[] appmntList = [SELECT AppointmentType__c, StartDateAndTime__c, EndDateAndTime__c, DentistUser__c, Contact__r.Phone, Contact__r.Email
	                  FROM Appointment__c WHERE Id = :appmntBookedId];
	        
	        if(appmntList.size() > 0) {
	        	appmnt = appmntList[0];
	        }
		}
		System.debug('appmnt = ' + appmnt);
		
	}
}