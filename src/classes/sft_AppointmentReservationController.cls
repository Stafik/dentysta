public with sharing class sft_AppointmentReservationController {

	public Id dentist { get; set; }
	public Appointment__c appointmentToSave { get; set; }
	public Contact contactToSave { get; set; }
	private final Appointment__c appmnt;
    private Date startDate { get; set; }
    private Date stopDate { get; set; }
    public Boolean displayDetailSection { get; set; }	// set true if the 3rd section (Personal Data) shold be visible
    public List<sft_AppointmentDatesWrapper> slotList { get; set; }
    
    
    public sft_AppointmentReservationController() {
    	
    }
    
    public sft_AppointmentReservationController(ApexPages.StandardController stdController) {

		this.appmnt = (Appointment__c)stdController.getRecord();
		appointmentToSave = new Appointment__c();
		contactToSave = new Contact();
		
		startDate = Date.today();
		stopDate = startDate.addDays(7 - sft_AppointmentReservationHelper.calculateNumericDayOfTheWeek(startDate));	// calculates the last day of the current week
		
		System.debug('konstruktor: startDate = ' + startDate + ', stopDate = ' + stopDate);
		
			// when a new Reservation is booked the page reloads and displays a PageMessage confirmation
    	String showConfirmationBooked = ApexPages.currentPage().getParameters().get('conf');
    	if (showConfirmationBooked == 'true') {
    		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Appointment has been created successfully.');
        	ApexPages.addMessage(msg);
    	}

    }

    public List<SelectOption> getListOfDentists() {

        List<SelectOption> dentists = new List<SelectOption>();

        for (User dent : [SELECT Id, FirstName, LastName FROM User WHERE DentistType__c != null]) {
            dentists.add(new SelectOption(dent.Id, dent.FirstName + ' ' + dent.LastName));
        }
        return dentists;
    }
    
    
    public void getAvailableSlotList() {
    	
    	sft_CalculateHours calcHrs = new sft_CalculateHours();
    	slotList = calcHrs.getSlots(dentist, startDate, stopDate);
    	
    	System.debug('dentist = ' + dentist + ', startDate = ' + startDate + ', stopDate = ' + stopDate);
    	System.debug('slotList final = ' + slotList);
		
    }
    
    public PageReference slotListPreviousWeek() {
    	
    	// System.debug('Prev Week: startDate = ' + startDate + ', stopDate = ' + stopDate);
    	if (startDate.addDays(-7) < Date.today())
    	{
    		// can't go back to the past so let's start afresh
    		startDate = Date.today();
    		stopDate = startDate.addDays(7 - sft_AppointmentReservationHelper.calculateNumericDayOfTheWeek(startDate));
    	} else {
    		this.startDate = startDate.addDays(-7);
    		this.stopDate = stopDate.addDays(-7);
    	}

    	// System.debug('Prev Week -6: startDate = ' + startDate + ', stopDate = ' + stopDate);
    	getAvailableSlotList();
    	displayDetailSection = false;
    	
    	return null;
    }
    
    public void slotListNextWeek() {

    	this.startDate = stopDate.addDays(1);
    	this.stopDate = stopDate.addDays(7);
    	getAvailableSlotList();
    	displayDetailSection = false;
    }

    public PageReference bookSelectedDate() {
    	
    	sft_AppointmentDatesWrapper.InnerDate selectedDate = getSelectedDate(slotList);
    	System.debug('selectedDate1 = ' + selectedDate);
    	if (selectedDate != null) {
    		System.debug('selectedDate = ' + selectedDate);
    		displayDetailSection = true;	// shows the 3rd pageblocksection (Personal data form)
    		
    		appointmentToSave.StartDateAndTime__c = Datetime.newInstanceGMT(selectedDate.startDateTime.date(), selectedDate.startDateTime.time());
    		appointmentToSave.EndDateAndTime__c = Datetime.newInstanceGMT(selectedDate.stopDateTime.date(), selectedDate.stopDateTime.time());
    		appointmentToSave.DentistUser__c = this.dentist;
    		appointmentToSave.AppointmentStatus__c = sft_ConfigArray.AWAITING_CONFIRMATION;
    		appointmentToSave.TemporaryReservation__c = true;	// let's mark it a temporary booking
    		appointmentToSave.Name = 'Temporary Reservation';
    		
    			// let's make a temporary reservation
    		try {
    			insert appointmentToSave;
    		}
    		catch (DmlException e) {
    			System.debug('Can\'t insert temporary reservation. ' + e.getMessage());
    		}
    	}
    	else {
    		displayDetailSection = false;	// hides the 3rd pageblocksection (Personal data form)
    	}
    	
    	return null;
    }
    
    public PageReference bookAppointment() {
    	
    	PageReference pageAppomntBookingConfirmation;
    	
    		// let's get selectedDate once again to make it sure the user hasn't ticked another checkbox meanwhile (appointment's date)
    	sft_AppointmentDatesWrapper.InnerDate selectedDate = getSelectedDate(slotList);
    	
    	System.debug('appointmentToSave = ' + appointmentToSave);
    	
    		// Let's validate all the fields against being empty
    	sft_AppointmentReservationHelper.validateFields(appointmentToSave, contactToSave);
    	
    		// if there are validation errors
    	if (!ApexPages.hasMessages()) {
    			// I don't use 'LastName' required field from the beginning as it spoils rerendering the 3rd section (personal data)
    			// once 'Book selected date' is clicked.
    		contactToSave.LastName = contactToSave.LastName__c;
    		
    		insertAppointment(appointmentToSave, contactToSave);
    		
			pageAppomntBookingConfirmation = new PageReference('/apex/sft_AppointmentReservation');
			pageAppomntBookingConfirmation.getParameters().put('conf', 'true');
			pageAppomntBookingConfirmation.setRedirect(true);
			// it's not possible to redirect to a confirmation page as DE allows exposing only one public site
			//pageAppomntBookingConfirmation = new PageReference('/apex/sft_AppointmentReservationConfirmation');
			//pageAppomntBookingConfirmation.getParameters().put('id', appointmentToSave.Id);
    		
    	}
    	
    	return pageAppomntBookingConfirmation;
    }
    
    	// this method is also used to create an appointment via REST 
    public static Id insertAppointment(Appointment__c appmnt, Contact cont) {
    	
    	Id contactId;
    	
    	appmnt.Name = sft_AppointmentReservationHelper.getAppointmentsName(appmnt, cont);
    	appmnt.TemporaryReservation__c = false;
		contactId = sft_AppointmentReservationHelper.checkIfContactExists(cont.Email);
			
		try {
    		if (contactId != null) {
    			appmnt.Contact__c = contactId;
    			System.debug('contactId != null, ' + appmnt);
    		}
    		else {
    			insert cont;
    			appmnt.Contact__c = cont.Id;
    		}
    		System.debug('before update appointmentToSave: , ' + appmnt);
    		// it'll perform upsert once booking on the page and insert once calling REST service
			upsert appmnt; 
			sft_AppointmentReservationHelper.sendForApproval(appmnt);
			
		}
		catch (DmlException e) {
			System.debug('Something went wrong whilst updating a new appointment and contact: ' + e.getMessage());
		}
		
		return appmnt.Id;
    }
     
    public sft_AppointmentDatesWrapper.InnerDate getSelectedDate(List<sft_AppointmentDatesWrapper> slotList) {
    	System.debug('slotList = ' + slotList);
    	sft_AppointmentDatesWrapper.InnerDate result;
    	System.debug('result = ' + result);
    	
    	for (sft_AppointmentDatesWrapper adw : slotList) {
    		System.debug('result w pierwszym for = ' + result);
    		for (sft_AppointmentDatesWrapper.InnerDate id : adw.listOfInnerDates) {
    			System.debug('id.slotPickedToSave = ' + id.slotPickedToSave);
    			System.debug('result w drugim for = ' + result);
        		if (id.slotPickedToSave == true) {
        			System.debug('id = ' + id);
        			System.debug('result w if = ' + result);
        			result = id;
        		}
    		}
    	}
    	System.debug('przed result = ' + result);
    	return result;
    }
    
    	// this function is tied to "choose a date" the checkbox and handles the temporary reservation if a user changes the booking date
    public void changeBookedDate() {
    		// hides the 3rd section - personal data
    	displayDetailSection = false;
    		
    		// Id != null means that a temporary reservation has been already done (user is now changing the date, not picking it for the first time)
    	if (appointmentToSave.Id != null ) {
    		try {
    			delete appointmentToSave;
    			appointmentToSave.Id = null;	// let's set the reference to null as the appointment no longer exists
    		}
    		catch (DmlException e) {
    			System.debug('Can\'t delete the temporary reservation. ' + e.getMessage());
    		}
    	}
    	// else do nothing as bookSelectedDate() will reset the appointmentToSave from scratch
    }
}