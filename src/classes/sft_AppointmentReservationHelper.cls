public class sft_AppointmentReservationHelper {
        
    public static Integer calculateNumericDayOfTheWeek(Date dateInQuestion) {

    	DateTime tempDate = DateTime.newInstance(dateInQuestion.year(), dateInQuestion.month(), dateInQuestion.day());
    		// Gets the day of the week using non-well documented Java 'format' method. 
    		// It doesn't depend on locale (e.g. Sunday is the first day of the week in US, contrary to Europe).
    		// format('u') is available only for DateTime class.
    	Integer result = Integer.valueOf(tempDate.format('u'));

    	return result;
    }
    
    public static List<sft_AppointmentDatesWrapper> convertWorkingHoursExceptionsToAppDates(WorkingHoursException__c[] workingHoursExceptionList) {
        
        List<sft_AppointmentDatesWrapper> result = new List<sft_AppointmentDatesWrapper>();
        Map<Date, sft_AppointmentDatesWrapper> tempContainer = new Map<Date, sft_AppointmentDatesWrapper>();
        sft_AppointmentDatesWrapper adw;
        
    		// convert SOQL result into ReturnedDates objects and aligning with the current date
        for (WorkingHoursException__c whel : workingHoursExceptionList) {
            Integer weekDay = sft_AppointmentReservationHelper.calculateNumericDayOfTheWeek(whel.StartTime__c.date());
            	// checks if there is an object of this day in the Map (in case there are two exceptions the same day), if not it creates a new one
            if (tempContainer.containsKey(whel.StartTime__c.date())) {
            	tempContainer.get(whel.StartTime__c.date()).addInnerDate(whel.StartTime__c, whel.StopTime__c);
            }
            else {
            		// let's create a new wrapper and add it to the container
            	adw = new sft_AppointmentDatesWrapper();
            	adw.dayOfWeek = weekDay;
            	adw.addInnerDate(whel.StartTime__c, whel.StopTime__c);
            	tempContainer.put(whel.StartTime__c.date(), adw);
            }
        }
        result = tempContainer.values();
        
        return result;
    }
    
    public static List<sft_AppointmentDatesWrapper> convertAppointmentsToAppDates(Appointment__c[] appointmentList) {
        
        List<sft_AppointmentDatesWrapper> result = new List<sft_AppointmentDatesWrapper>();
        Map<Date, sft_AppointmentDatesWrapper> tempContainer = new Map<Date, sft_AppointmentDatesWrapper>();
        sft_AppointmentDatesWrapper adw;
        
    		// convert SOQL result into ReturnedDates objects and aligning with the current date
        for (Appointment__c whel : appointmentList) {
            Integer weekDay = sft_AppointmentReservationHelper.calculateNumericDayOfTheWeek(whel.StartDateAndTime__c.date());
            	// checks if there is an object of this day in the Map (in case there are two exceptions the same day), if not it creates a new one
            if (tempContainer.containsKey(whel.StartDateAndTime__c.date())) {
            	tempContainer.get(whel.StartDateAndTime__c.date()).addInnerDate(whel.StartDateAndTime__c, whel.EndDateAndTime__c);
            }
            else {
            		// let's create a new wrapper and add it to the container
            	adw = new sft_AppointmentDatesWrapper();
            	adw.dayOfWeek = weekDay;
            	adw.addInnerDate(whel.StartDateAndTime__c, whel.EndDateAndTime__c);
            	tempContainer.put(whel.StartDateAndTime__c.date(), adw);
            }
        }
        result = tempContainer.values();
        
        return result;
    }
    
    	// function validates all fields against being empty
    public static void validateFields(Appointment__c app, Contact con) {
    	

    	if (app.AppointmentType__c == null || app.AppointmentType__c == '') {
    		addErrorMessage('Appointment__c', 'AppointmentType__c');
    		System.debug('error in AppointmentType__c');
    	}

    	if (con.FirstName == null || con.FirstName == '') {
    		addErrorMessage('Contact', 'FirstName');
    		System.debug('error in FirstName');
    	}
    	if (con.LastName__c == null || con.LastName__c == '') {
    		addErrorMessage('Contact', 'LastName__c');
    		System.debug('error in LastName__c');
    	}
    	if (con.Phone == null || con.Phone == '') {
    		addErrorMessage('Contact', 'Phone');
    		System.debug('error in Phone');
    	}
    	if (con.Email == null || con.Email == '') {
    		addErrorMessage('Contact', 'Email');
    		System.debug('error in Email');
    	}
    	if (app.AppointmentType__c == 'Leczenie' && (con.Description == null || con.Description == '')) {
    		addErrorMessage('Contact', 'Description');
    		System.debug('error in Description');
    	}
    }
    
    public static void addErrorMessage(String objName, String nameField) {
    	
    	String fieldLabel, msgTextInfo;
    	
    	fieldLabel = Schema.getGlobalDescribe().get(objName).getDescribe().fields.getMap().get(nameField).getDescribe().getLabel();
    	msgTextInfo = 'Please fill out the following field: ' + fieldLabel;
    	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, msgTextInfo);
    	ApexPages.addMessage(msg);
    	System.debug('msg = ' + msg);
    }
    
    public static Id checkIfContactExists(String emailAddress) {
    	
    	Contact[] con = [SELECT Email FROM Contact WHERE Email = :emailAddress LIMIT 1];
    	
    	if (con.size() != 0) {
    		return con[0].Id;
    	}
    	else {
    		return null;
    	}
    }
    
    public static String getAppointmentsName(Appointment__c app, Contact con) {
    	
    	String result = '';	// e.g.: AB_MK_20160812_1530
    	String character;
    	
    	User dentist = [SELECT FirstName, LastName FROM User WHERE Id = :app.DentistUser__c];
    	
    	result = dentist.FirstName.substring(0,1).toUpperCase() +
    			dentist.LastName.substring(0,1).toUpperCase() +
    			'_' +
    			con.FirstName.substring(0,1).toUpperCase() + 
    			con.LastName__c.substring(0,1).toUpperCase() +
    			'_' +
    			String.valueOf(app.StartDateAndTime__c.yearGMT()) +
    			String.valueOf(app.StartDateAndTime__c.monthGMT()) +
    			String.valueOf(app.StartDateAndTime__c.dayGMT()) +
    			'_' +
    			String.valueOf(app.StartDateAndTime__c.hourGMT());
    	
    			// if the minute component is less than 10 then only one digit is returned
    	result += (app.StartDateAndTime__c.minuteGMT() < 10) ?
    			( '0' + String.valueOf(app.StartDateAndTime__c.minuteGMT()) ) :
    			String.valueOf(app.StartDateAndTime__c.minuteGMT());
    	
    	System.debug('result (AB_MK_20160812_1530) = ' + result);

    	return result;
    }
    
    	// sends the Appointment__c record for approval
    public static void sendForApproval(Appointment__c app) {
    	
    	User submitter = [SELECT Id FROM User WHERE Alias = 'PKowa'];
    	String approvalProcessName;
    	Time eightoclock = Time.newInstance(20, 0, 0, 0);
    	Integer dayOfWeek = calculateNumericDayOfTheWeek(app.EndDateAndTime__c.date());
    	
    		// if it's Sunday or past 20:00 submit the record for special approval
    	if (app.EndDateAndTime__c.time() > eightoclock || dayOfWeek == 7) {
    		approvalProcessName = 'Special_appointment_approval';
    	}
    	else {
    		approvalProcessName = 'Ordinary_appointment_approval';
    	}
    	
    		// create an approval request
    	Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
    	request.setComments('Submitting for approval');
    	request.setObjectId(app.Id);
    	request.setSubmitterId(submitter.Id);
    	request.setProcessDefinitionNameOrId(approvalProcessName);
    	request.setSkipEntryCriteria(true);
    	
    		// submit the approval request
    	Approval.ProcessResult result = Approval.process(request);
    	System.debug('Approval.process result = ' + result);
    	
    }
}