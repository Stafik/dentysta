public class sft_AppointmentsListController {
   
    public Appointment__c appmnt { get; set; }
    public User usr { get; set; }
    public List<Appointment__c> listOfAppmnts { get; set; }
    public Id currentAppmntId { get; set; }
    private Id userId;
    private String profileName;
    private final String queryProto;
    
    
    public sft_AppointmentsListController() {
	
    	String query;
    	appmnt = new Appointment__c();
    	usr = new User();
    	userId = UserInfo.getUserId();
    		// get User's Profile
    	profileName = [SELECT Id, Name FROM Profile WHERE Id = :UserInfo.getProfileId() LIMIT 1][0].Name;
    		// a query which will have some more conditions added later
    	queryProto = 'SELECT Name, AppointmentType__c, AppointmentStatus__c, StartDateAndTime__c, EndDateAndTime__c, ' + 
				' Contact__r.Id, Contact__r.Name, DentistUser__r.Id, DentistUser__r.Name ' +
				'FROM Appointment__c WHERE TemporaryReservation__c = false ';
    	
    	query = queryProto;
    		// let's filter using dentist's Id so that he/she sees only their records OR no filtering in case of the owner or sys admin
    		// if it's the owner, he/she can see everything so check Doctor's Id against non existing value that gives always true
    	if (profileName != sft_ConfigArray.OWNER_PROFILE_NAME && profileName != sft_ConfigArray.SYS_ADMIN_PROFILE_NAME) {
    		// it's an ordinary dentist so let's filter the results down to his/her Appointments
    		query += ' AND DentistUser__r.Id = \'' + userId + '\' ';
    	}
    	
    	listOfAppmnts = Database.query(query);
    }
    
    public PageReference displayTable() {
    	
    	String query = buildQuery();
    	
    	listOfAppmnts = Database.query(query);
    	System.debug('listOfAppmnts: ' + listOfAppmnts);
    	
    	return null;
    }
    
    public PageReference showDetailPage() {
    	
    	PageReference detailPage = new PageReference('/apex/sft_AppointmentDetail');
    	detailPage.getParameters().put('id', currentAppmntId);
    	
    	System.debug('currentAppmntId: ' + currentAppmntId);
    	
    	return detailPage;
    }
    
    public PageReference save() {
    	
    	try {
    		update listOfAppmnts;
    	}
    	catch (DmlException e) {
    		System.debug('Can\'t update listOfAppmnts edited inline in sft_AppointmentsList page: ' + e.getMessage());
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Can\'t update the list of Appointments edited inline.'));
    	}
    	return null;
    }
    
     
    private String buildQuery() {
    	
    	String query = queryProto;
    	
    		// In case of the owner or sys admin don't filter by dentists but show them all (hence there's no another 'WHERE' condition here).
    	if (profileName == sft_ConfigArray.OWNER_PROFILE_NAME || profileName == sft_ConfigArray.SYS_ADMIN_PROFILE_NAME) {
    		
    			// besides filter the dentist's names as well;  'FirstName' field is used in VF as 'Name' cannot be used. Both fits in here well.
    		if (usr.FirstName != null) {
    			query += ' AND DentistUser__r.Name LIKE \'%' + usr.FirstName + '%\'';
        	}
    		if (usr.DentistType__c != null) {
    			query += ' AND DentistUser__r.DentistType__c = \'' + usr.DentistType__c + '\'';
    		}
    	}
    	else {
    		// Dentist here so fetch all his/her Appointments and go on with the rest of filters
    		query += ' AND DentistUser__r.Id = \'' + userId + '\' ';
    	}
    	if (appmnt.Name != null) {
    		query += ' AND Contact__r.Name LIKE \'%' + appmnt.Name + '%\'';
    	}
    	if (appmnt.AppointmentType__c != null) {
    		query += ' AND AppointmentType__c = \'' + appmnt.AppointmentType__c + '\'';
    	}
    	if (appmnt.AppointmentStatus__c != null) {
    		query += ' AND AppointmentStatus__c = \'' + appmnt.AppointmentStatus__c +'\'';
    	}
    	
    	System.debug('build Query result: ' + query);
    	
    	return query;
    }
}