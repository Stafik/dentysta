public class sft_CalculateHours {

	
		// calculates free slots for a given dentist Id, startDate and stopDate
	public List<sft_AppointmentDatesWrapper> getSlots(Id dentist, Date startDate, Date stopDate) {
		
		List<sft_AppointmentDatesWrapper> wrkHrsWithDates,
										  wrkHrsExc,
										  wrkHrsWithExc,
										  wrkHrsSlots,
										  alreadyBookedSlots,
										  availHrsSlots;
		
		WorkingHours__c[] wrkHrs = getStandardWorkingHours(dentist);
		wrkHrsWithDates = getStandardWorkingHoursWithDates(startDate, stopDate, wrkHrs);
		wrkHrsExc = getWorkingHoursExceptions(dentist, startDate, stopDate);
		wrkHrsWithExc = getWorkingHoursWithExceptions(wrkHrsWithDates, wrkHrsExc);
		wrkHrsSlots = getWorkingHoursSlots(wrkHrsWithExc);
		alreadyBookedSlots = getAlreadyBookedSlots(dentist, startDate, stopDate);
		availHrsSlots = getAvailableHourSlots(wrkHrsSlots, alreadyBookedSlots);
		
		System.debug('*** availHrsSlots = ' + availHrsSlots + ' ***');
		
		return availHrsSlots;
	}
	
	
	public WorkingHours__c[] getStandardWorkingHours(Id dentist) {
        
        	// fetch normal working hours for a given dentist
        WorkingHours__c[] workingHoursList = Database.query('SELECT DayOfTheWeek__c, StartTime__c, StopTime__c ' + 
        													'FROM WorkingHours__c WHERE DentistUser__c = :dentist');
        
        return workingHoursList;
    }
	
	
	private List<sft_AppointmentDatesWrapper> getStandardWorkingHoursWithDates(Date startDate, Date stopDate, WorkingHours__c[] stdWorkingHours) {

    	List<sft_AppointmentDatesWrapper> result = new List<sft_AppointmentDatesWrapper>();
    	Integer weekDay;
    	Datetime dateStart, dateStop;
    	
    	for (Date currentDate = startDate; currentDate <= stopDate; currentDate = currentDate.addDays(1)) {
    		//System.debug('for dla currentDate = ' + currentDate);
    		weekDay = sft_AppointmentReservationHelper.calculateNumericDayOfTheWeek(currentDate);
    		
    			// let's create a Wrapper for every day(date)
    		sft_AppointmentDatesWrapper adw = new sft_AppointmentDatesWrapper();
    		adw.dayOfWeek = weekDay;
    		//System.debug('stdWorkingHours = ' + stdWorkingHours);
    		
    		for (WorkingHours__c wh : stdWorkingHours) {
    			//System.debug('drugi for dla wh = ' + wh);
    			
    				// for each workingHour entry (might be more than one per day) if this meets the current week day
    			if (wh.DayOfTheWeek__c == weekDay) {
    				dateStart = Datetime.newInstanceGmt(currentDate.year(), currentDate.month(), currentDate.day()).addMinutes(wh.StartTime__c.intValue());
    				dateStop = Datetime.newInstanceGmt(currentDate.year(), currentDate.month(), currentDate.day()).addMinutes(wh.StopTime__c.intValue());
    				adw.addInnerDate(dateStart, dateStop);
    			}
    		}
    			// if workingHours exist for current day then add it to the result
    		if (adw.listOfInnerDates.size() != 0) 
    			result.add(adw);
    	}
    	
    	//System.debug(result);
    	
        return result;
    }
	
		
	private List<sft_AppointmentDatesWrapper> getWorkingHoursExceptions(Id dentist, Date startDate, Date stopDate) {
        
        List<sft_AppointmentDatesWrapper> result = new List<sft_AppointmentDatesWrapper>();
        Datetime startDateToBind = Datetime.newInstance(startDate.year(), startDate.month(), startDate.day());
        Datetime endDateToBind = Datetime.newInstance(stopDate.year(), stopDate.month(), stopDate.day());
        System.debug('startDateToBind = ' + startDateToBind + ', stopDateToBind = ' + endDateToBind);
        WorkingHoursException__c[] workingHoursExceptionList = Database.query('SELECT StartTime__c, StopTime__c ' +
        															'FROM WorkingHoursException__c WHERE DentistUser__c = :dentist AND ' +
        															'StartTime__c >= :startDateToBind AND StopTime__c < :endDateToBind');
        System.debug('workingHoursExceptionList = ' + workingHoursExceptionList);
        result = sft_AppointmentReservationHelper.convertWorkingHoursExceptionsToAppDates(workingHoursExceptionList);
        System.debug('workingHoursExceptionList = ' + result);
        
        return result;
    }
    
    
	private List<sft_AppointmentDatesWrapper> getWorkingHoursWithExceptions(List<sft_AppointmentDatesWrapper> stdWorkingHoursWithDates, List<sft_AppointmentDatesWrapper> workingHoursExceptions) {
        
    	Map<Date, sft_AppointmentDatesWrapper> stdWorkingHoursMap = new Map<Date, sft_AppointmentDatesWrapper>();
    	Map<Date, sft_AppointmentDatesWrapper> workingHrsExceptions = new Map<Date, sft_AppointmentDatesWrapper>();
    	List<sft_AppointmentDatesWrapper> result = new List<sft_AppointmentDatesWrapper>();
    	
    		// Let's create two maps and compare their keys (effectively the dates)
    	for(sft_AppointmentDatesWrapper wrkHours : stdWorkingHoursWithDates) {
    		stdWorkingHoursMap.put(wrkHours.getFirstDate(), wrkHours);
    	}
    	for(sft_AppointmentDatesWrapper wrkHoursExcep : workingHoursExceptions) {
    		workingHrsExceptions.put(wrkHoursExcep.getFirstDate(), wrkHoursExcep);
    	}
    		// for each exception let's check if such date exists in stdWorkingHours. If so, replace the working hours for the date given.
    	for(Date dateKey : workingHrsExceptions.keySet()) {
    		if (stdWorkingHoursMap.containsKey(dateKey)) {
    			stdWorkingHoursMap.get(dateKey).replaceInnerDates(workingHrsExceptions.get(dateKey).listOfInnerDates);
    		}
    	}
    	
    	result = stdWorkingHoursMap.values();
        System.debug('workingHoursWithExceptions = ' + result);
        return result;
    }
    
	private List<sft_AppointmentDatesWrapper> getWorkingHoursSlots(List<sft_AppointmentDatesWrapper> workingHrsWithExceptions) {
        
    	List<sft_AppointmentDatesWrapper> result = new List<sft_AppointmentDatesWrapper>(workingHrsWithExceptions);
    		// get configuration and Appointment's Length value
    	List<sft_Configuration__c> config = sft_Configuration__c.getAll().values();
    	Integer appointmentLength = Integer.valueOf(config[0].get('Appointment_s_Length__c'));
    	
    	for (sft_AppointmentDatesWrapper adw : result) {
    		adw.makeWorkingSlots(appointmentLength);
    	}

    	//System.debug(result); 
    	System.debug('result w getWorkingHoursSlots = ' + result);
        return result;
    }
    
    
	private List<sft_AppointmentDatesWrapper> getAlreadyBookedSlots(Id dentist, Date startDate, Date endDate) {
        
        Datetime startDateToBind = Datetime.newInstance(startDate.year(), startDate.month(), startDate.day());
        Datetime stopDateToBind = Datetime.newInstance(endDate.year(), endDate.month(), endDate.day(), 23, 59, 59);
        List<sft_AppointmentDatesWrapper> result = new List<sft_AppointmentDatesWrapper>();
        List<Appointment__c> appointmentList = new List<Appointment__c>();
        
        appointmentList = [SELECT DayOfWeek__c, StartDateAndTime__c, EndDateAndTime__c FROM Appointment__c
                           WHERE DentistUser__c = :dentist 
                           AND StartDateAndTime__c >= :startDateToBind
                           AND EndDateAndTime__c <= :stopDateToBind];
        System.debug('appointmentList = ' + appointmentList);
        result = sft_AppointmentReservationHelper.convertAppointmentsToAppDates(appointmentList); 
        
        System.debug('AlreadyBookedSlots result: ' + result);
        return result;
    }
    
	private List<sft_AppointmentDatesWrapper> getAvailableHourSlots(List<sft_AppointmentDatesWrapper> wrkHoursWithExcep,
    																List<sft_AppointmentDatesWrapper> bookedSlots) {
        
        List<sft_AppointmentDatesWrapper> result = new List<sft_AppointmentDatesWrapper>();
    	Map<Date, sft_AppointmentDatesWrapper> wrkHoursWithExcepMap = new Map<Date, sft_AppointmentDatesWrapper>();
    	Map<Date, sft_AppointmentDatesWrapper> bookedSlotsMap = new Map<Date, sft_AppointmentDatesWrapper>();

		
			// Let's create two maps and compare slots for the same dates
		for (sft_AppointmentDatesWrapper wrkHours : wrkHoursWithExcep) {
				// this 'if' checks if any working slots have been created at all (there might be a situation where the slot length (taken from the configuration) is longer
				// than the working time (e.g. 12:00-12:25 whereas slot length = 30 min)
			if (wrkHours.listOfInnerDates.size() > 0) {
				wrkHoursWithExcepMap.put(wrkHours.getFirstDate(), wrkHours);
			}
		}
		for (sft_AppointmentDatesWrapper wrkHoursExcep : bookedSlots) {
			bookedSlotsMap.put(wrkHoursExcep.getFirstDate(), wrkHoursExcep);
		}
			
			// for each day that has at least one booked slot:
		for (Date bookedSlotsKey : bookedSlotsMap.keySet()) {
				// try to find a matching day and remove booked slots
			if (wrkHoursWithExcepMap.containsKey(bookedSlotsKey)) {
				wrkHoursWithExcepMap.get(bookedSlotsKey).removeAlreadyBookedSlots(bookedSlotsMap.get(bookedSlotsKey).listOfInnerDates);
			}
		}
		
		result = wrkHoursWithExcepMap.values();

        return result;
    }

	public Boolean canAppmntBeBooked(Id dentist, Datetime startDate) {
		
		List<sft_AppointmentDatesWrapper> freeSlots;
		Boolean result = false;
		
			// let's get free available slots
		freeSlots = getSlots(dentist, startDate.date(), startDate.date());
		for (sft_AppointmentDatesWrapper adw : freeSlots) {
			for (sft_AppointmentDatesWrapper.InnerDate innDate : adw.listOfInnerDates) {
				System.debug ('innDate.startDateTime == startDate: ' + innDate.startDateTime + ' == ' + startDate + '?');
				if (innDate.startDateTime == startDate) {
					result = true;
				}
			}
		}
		return result;
	}
}