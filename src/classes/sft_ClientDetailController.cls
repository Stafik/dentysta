public class sft_ClientDetailController {
	
    public Id clientId { get; set; }
    public Contact client { get; set; }
    public Appointment__c[] appmnts { get; set; }
    public Id relatedAppmntId { get; set; }
    
    public sft_ClientDetailController() {
    	
    	clientId = ApexPages.currentPage().getParameters().get('id');
    	if (clientId != null) {
    		client = [SELECT Name, Email, Phone FROM Contact WHERE Id = :clientId];
    		appmnts = [SELECT Name, StartDateAndTime__c, EndDateAndTime__c, DentistUser__c FROM Appointment__c WHERE Contact__c = :clientId];
    	}
    	System.debug('clientId = ' + clientId + ', client = ' + client);
    	System.debug('appmnts = ' + appmnts);
    	
    }
    
    public PageReference goToDetailPage() {
    	
    	PageReference appDetailPage = new PageReference('/apex/sft_AppointmentDetail');
    	appDetailPage.getParameters().put('id', relatedAppmntId);
    	System.debug('relatedAppmntId = ' + relatedAppmntId);
    	
    	return appDetailPage;
    }
}