public class sft_ClientsListController {
	
    public Contact[] clientsList { get; set; }
    public Id clientId { get; set; }
    
    public sft_ClientsListController() {
    	
    	clientsList = [SELECT Id, Name, Phone, Email FROM Contact];
    }
    
    public PageReference showDetailPage() {
    	
    	PageReference clientDetails = new PageReference('/apex/sft_ClientDetail');
    	clientDetails.getParameters().put('id', clientId);
    	System.debug('clientId = ' + clientId);
    	
    	return clientDetails;
    }
}