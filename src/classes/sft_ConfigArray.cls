public class sft_ConfigArray {
    
		/*** PROFILE NAMES ***/
	public static final String OWNER_PROFILE_NAME = 'Owner';
	public static final String DENTIST_PROFILE_NAME = 'Dentist';
	public static final String SYS_ADMIN_PROFILE_NAME = 'System Administrator';
	
		/*** WEEKDAY NAMES ***/
	public static final String MONDAY = 'Monday';
	public static final String TUESDAY = 'Tuesday';
	public static final String WEDNESDAY = 'Wednesday';
	public static final String THURSDAY = 'Thursday';
	public static final String FRIDAY = 'Friday';
	public static final String SATURDAY = 'Saturday';
	public static final String SUNDAY = 'Sunday';
	
		/*** APPOINTMENT STATUSES ***/
	public static final String AWAITING_CONFIRMATION = 'Pending approval';
	public static final String ACCEPTED = 'Accepted';
	public static final String REJECTED = 'Refused';
	
		/*** APPOINTMENT TYPES ***/
	public static final String CHECKUP = 'check-up';
	public static final String TREATMENT = 'treatment';
	
    public static final Map<String, Integer> dayOfWeekToIntegerMap = new Map<String, Integer> {
        MONDAY => 1, TUESDAY => 2, WEDNESDAY => 3, THURSDAY => 4,
        FRIDAY => 5, SATURDAY => 6, SUNDAY => 7 };
        
    public static final Map<Decimal, String> integerToDayOfWeekMap = new Map<Decimal, String> {
        1 => MONDAY, 2 => TUESDAY, 3 => WEDNESDAY, 4 => THURSDAY,
        5 => FRIDAY, 6 => SATURDAY, 7 => SUNDAY };
        
        /*** CONFIGURATION EDIT ERROR MSGS ***/
    public static final String APPOINTMENT_LENGTH_EXCEEDED = 'Appointment length has to be greater than 5 and lesser than 1440.';
    public static final String TEMPORARY_RESERVATION_PERIOD = 'Temporary Reservation Period value has to be beetween 5 and 30 minutes.';
    
}