public class sft_ConfigurationEditController {
	
	public Integer appointmentLength { get; set; }
	List<sft_Configuration__c> config;
	public String timeZone;
	private sft_Configuration2__c config2;
	public Integer tempReservationPeriod { get; set; }
	public WorkingHours__c workingHoursItem { get; set; }
	public WorkingHours__c[] workingHoursList;
	public WorkingHoursException__c workingHoursExceptionsItem { get; set; }
	public WorkingHoursException__c[] workingHoursExceptionsList;
	public SelectOption[] dayOfWeekList;
	
	
	public sft_configurationEditController() {
		
		config = sft_Configuration__c.getAll().values();
		System.assert(config.size() == 1);
		
		appointmentLength = Integer.valueOf(config[0].get('Appointment_s_Length__c'));
		
			// fetch TempReservationPeriod from Hierarchy Custom Settings
		config2 = sft_Configuration2__c.getOrgDefaults();
		Decimal decimal_tempReservationPeriod = config2.TempReservationPeriod__c;
		tempReservationPeriod = Integer.valueOf(decimal_tempReservationPeriod);
		
		workingHoursItem = new WorkingHours__c();
		WorkingHoursExceptionsItem = new WorkingHoursException__c();
		
		workingHoursList = getWorkingHoursList();
		workingHoursExceptionsList = getWorkingHoursExceptionsList();
	}
	
	public List<SelectOption> getTimeZoneList() {
		
		List<SelectOption> options = new List<SelectOption>();
		
		Schema.DescribeFieldResult fieldResult = User.TimeZoneSidKey.getdescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for(Schema.PicklistEntry p : ple)
		{
			String value = p.getLabel().substringBetween('(GMT', ') ');
			options.add(new SelectOption(p.getValue(), p.getlabel()));
		}
		
		return options;
	
	}

	public String getTimeZone() {
		
		String tz = String.valueOf(config[0].get('Time_Zone__c'));
		
		return tz;
	}
	
	public void setTimeZone(String tz) {
		
		this.timeZone = tz;
	}

	
	public PageReference save() {
		
			// let's validate input values
		isInputValid();
		
			// check if there are any validation errors
		if (ApexPages.hasMessages()) {
			return null;
		}
		else {
			config[0].put('Appointment_s_Length__c', appointmentLength);
			config[0].put('Time_Zone__c', timeZone);
			System.debug('timeZone: ' + timeZone);
			
			Decimal decimal_tempReservationPeriod = Decimal.valueOf(tempReservationPeriod);
			config2.TempReservationPeriod__c = decimal_tempReservationPeriod;
			
			try {
				update config;
				update config2;
			}
			catch (DmlException e) {
				System.debug('Cannot update sft configuration. ' + e.getMessage());
			}
			
			return Page.sft_Configuration;
		}
	}

	public PageReference cancel() {
		
		return Page.sft_Configuration;
	}
	
		// validates input values
	private void isInputValid() {
		
		if (appointmentLength < 5 || appointmentLength >= 1440) {
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, sft_ConfigArray.APPOINTMENT_LENGTH_EXCEEDED));
		}
		
		if (tempReservationPeriod < 5 || tempReservationPeriod >= 30) {
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, sft_ConfigArray.TEMPORARY_RESERVATION_PERIOD));
		}
	}
	
	/*********************************
	 * Working Hours Weekly          *
	 *********************************/
	
	public PageReference addNewWorkingHoursItem() {
		
		System.debug('workingHoursItem.StartTime__c: ' + workingHoursItem.StartTime__c + '; workingHoursItem.StopTime__c: ' + workingHoursItem.StopTime__c);
		
		System.debug('workingHoursItem.Name = ' + workingHoursItem.Name + ', workingHoursExceptionsItem.Name = ' + workingHoursExceptionsItem.Name);
		validateWorkingHoursItem();

			// Name fields are used here as the fancy JS timepicker returns a string that has to be converted to number of minutes past midnight.
			// The only available sObject String fields that are not used at all are Name fields so we use them here temporarily. 
		//workingHoursItem.StartTime__c = convertTimeToInt(workingHoursItem.Name);
		//workingHoursItem.StopTime__c = convertTimeToInt(workingHoursExceptionsItem.Name);
		
			// check if any validation method has risen errors?
		if (!ApexPages.hasMessages()) {

			workingHoursItem.DentistUser__c = UserInfo.getUserId();
			
			try {
				insert workingHoursItem;
					// let's clear the fields out (both workingHours and workingHoursException as there's Name field used of the latter one
				workingHoursItem = new WorkingHours__c();
				workingHoursExceptionsItem = new WorkingHoursException__c();
			}
			catch (DmlException e) {
				System.debug('Can\'t insert a new Working Hours Item: ' + e.getMessage());
			}
			
		}
		
		return null;
	}
	
	public WorkingHours__c[] getWorkingHoursList() {
		
		WorkingHours__c[] whList;
		Id userId = UserInfo.getUserId();
		
		whList = [SELECT DayOfTheWeek__c, DentistUser__c, StartTime__c, StopTime__c
		          FROM WorkingHours__c
		          WHERE DentistUser__c = :userId
		          ORDER BY DayOfTheWeek__c, StartTime__c];
		
		return whList;
	}
	 
	private Integer convertTimeToInt(String timeVal) {
		
		String[] hourAndMinutes = timeVal.split(':');
		Integer HH = Integer.valueOf(hourAndMinutes[0]);
		Integer mm = Integer.valueOf(hourAndMinutes[1]);
		
		Integer result = 60 * HH + mm;
		System.debug('convertTimeToInt result: ' + result);
		return result;
	}
	
	private PageReference validateWorkingHoursItem() {
		
		Pattern timePattern = Pattern.compile('([01]?[0-9]|[2][0-3]):([0-5][0-9])');	// A pattern that checks 00:00-23:59 time format.
		Matcher timeMatcher;
		
		String[] emptyFields = new List<String>();
		String[] wrongFormatfields = new List<String>();
		
		if (workingHoursItem.DayOfTheWeek__c == null) {
			emptyFields.add('Day Of Week');
		}
			// check if the field is not null
		if (workingHoursItem.Name == null) {
			emptyFields.add('Start Time');
		}
			// not null so check if it matches time format
		else {
			timeMatcher = timePattern.matcher(workingHoursItem.Name);
			if (!timeMatcher.matches()) {
				wrongFormatFields.add('Start Time');
			}
				// let's check if the Start Date isn't later than the Stop date
			else if (convertTimeToInt(workingHoursItem.Name) > convertTimeToInt(workingHoursExceptionsItem.Name)) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Start time value can\'t be later than the Stop time value'));
			}
				// let's check also if the working time is long enough to create at least 1 working slot (>= appointmentLength)
			else if (convertTimeToInt(workingHoursExceptionsItem.Name) - convertTimeToInt(workingHoursItem.Name) < appointmentLength) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Working time has to be longer than the Appointment Length value (' + appointmentLength + ' min)'));
			}
				// apparently it does so check if the new working time doesn't overlap any already existing
			else {
				workingHoursItem.StartTime__c = convertTimeToInt(workingHoursItem.Name);
				workingHoursItem.StopTime__c = convertTimeToInt(workingHoursExceptionsItem.Name);
				validateNewWorkingHoursValue();
			}
		}
		
		if (workingHoursExceptionsItem.Name == null) {
			emptyFields.add('Stop Time');
		}
		else {
			timeMatcher = timePattern.matcher(workingHoursExceptionsItem.Name);
			if (!timeMatcher.matches()) {
				wrongFormatFields.add('Stop Time');
			}
			else {
				workingHoursItem.StartTime__c = convertTimeToInt(workingHoursItem.Name);
				workingHoursItem.StopTime__c = convertTimeToInt(workingHoursExceptionsItem.Name);
				validateNewWorkingHoursValue();
			}
		}
		
		for (String fieldName : emptyFields) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill the following field out: ' + fieldName));
		}
		for (String fieldName : wrongFormatFields) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, fieldName + ' should have the following format: "HH:mm"'));
		}
		
		return null;
	}
	
	private void validateNewWorkingHoursValue() {
		
			// for all already existing Working Hours
		for (WorkingHours__c wh : workingHoursList) {
				// check if it's the same week day at all?
			if (workingHoursItem.DayOfTheWeek__c == wh.DayOfTheWeek__c) {
					// if so let's check if any new time value overlaps an already existing?
				if (workingHoursItem.StartTime__c >= wh.StartTime__c && workingHoursItem.StartTime__c < wh.StopTime__c) {
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Start time can\'t overlap an already existing working slot.'));
				}
				if (workingHoursItem.StopTime__c > wh.StartTime__c && workingHoursItem.StopTime__c <= wh.StopTime__c) {
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Stop time can\'t overlap an already existing working slot.'));
				}
			}
		}
	}
	
	/************************************
	 * Working Hours Exceptions         *
	 ************************************/
	
	public WorkingHoursException__c[] getWorkingHoursExceptionsList() {
		
		WorkingHoursException__c[] whexList;
		Id userId = UserInfo.getUserId();
		
		whexList = [SELECT DentistUser__c, StartTime__c, StopTime__c
		          FROM WorkingHoursException__c
		          WHERE DentistUser__c = :userId
		          ORDER BY StartTime__c];
		
		return whexList;
	}
	
	public PageReference addNewWorkingHoursExceptionItem() {
		
		System.debug('workingHoursExceptionsItem w addNewwhexItem: ' + workingHoursExceptionsItem);
		
		validateWorkingHoursExceptionsItem();
		
		if (!ApexPages.hasMessages()) {
			
			workingHoursExceptionsItem.DentistUser__c = UserInfo.getUserId();
			System.debug('workingHoursExceptionsItem w if po sprawdzeniu hasMessages: ' + workingHoursExceptionsItem);
			try {
				insert workingHoursExceptionsItem;
					// let's clean the fields out
				workingHoursExceptionsItem = new WorkingHoursException__c();
			}
			catch (DmlException e) {
				System.debug('Can\'t insert a new Working Hours Exception Item: ' + e.getMessage());
			}
		}
		return null;
	}
	
	private PageReference validateWorkingHoursExceptionsItem() {
		
		String[] emptyFields = new List<String>();
		String[] errorFields = new List<String>();
		System.debug('workingHoursExceptionsItem w validate: ' + workingHoursExceptionsItem);
		
		if (workingHoursExceptionsItem.StartTime__c == null) {
			emptyFields.add('Start Time');
		}
		if (workingHoursExceptionsItem.StopTime__c == null) {
			emptyFields.add('Stop Time');
		}
			// if StartTime__c and StopTime__c are not null let's do some more validation 
		if (emptyFields.size() == 0) {
				// let's check if Start Date isn't later than the Stop Date
			if (workingHoursExceptionsItem.StartTime__c > workingHoursExceptionsItem.StopTime__c) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Start time value can\'t be later than Stop time value'));
			}
				// let's check if the exception date doesn't span two or more days.
			else if (workingHoursExceptionsItem.StartTime__c.date() != workingHoursExceptionsItem.StopTime__c.date()) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Working hours can\'t extend to the next day (please add multiple exception records in such case)'));
			}
				// checks if the exception record isn't shorter than the appointment length
			else if (timeDifference(workingHoursExceptionsItem.StartTime__c.time(), workingHoursExceptionsItem.StopTime__c.time()) < appointmentLength) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Working hours can\'t be shorter than the appointment Length'));
			}
		}
		
		for (String fieldName : emptyFields) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill the following field out: ' + fieldName));
			System.debug('Exceptions empty fields: ' + fieldName);
		}
		
		return null;
	}
	
	private Integer timeDifference(Time t1, Time t2) {
		
		Integer hr, min, result;
		
		hr = t2.hour() - t1.hour();
		min = t2.minute() - t1.minute();
		
		result = 60 * hr + min;
		
		return result;
	}
	
		// return values for the Day Of Week picklist
	public SelectOption[] getDayOfWeekList() {
		
		SelectOption[] result = new List<SelectOption>();
		result.add(new SelectOption('1', sft_ConfigArray.MONDAY));
		result.add(new SelectOption('2', sft_ConfigArray.TUESDAY));
		result.add(new SelectOption('3', sft_ConfigArray.WEDNESDAY));
		result.add(new SelectOption('4', sft_ConfigArray.THURSDAY));
		result.add(new SelectOption('5', sft_ConfigArray.FRIDAY));
		result.add(new SelectOption('6', sft_ConfigArray.SATURDAY));
		result.add(new SelectOption('7', sft_ConfigArray.SUNDAY));
		
		return result;
	}
	
	public PageReference deleteWorkingHoursItem() {
		
		Id delWorkingHoursId = ApexPages.currentPage().getParameters().get('delWorkingHoursId');
		System.debug('delWorkingHoursId = ' + delWorkingHoursId);
		if (delWorkingHoursId != null) {
			try {
				Database.delete(delWorkingHoursId);
			}
			catch (DmlException e) {
				System.debug('Can\'t delete a Working Hours Item/ Working Hours Exceptions Item: ' + e.getMessage());
			}
		}
		return null;
	}
}