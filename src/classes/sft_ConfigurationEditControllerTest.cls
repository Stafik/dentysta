@isTest
class sft_ConfigurationEditControllerTest {
    
	@testSetup static void setupData() {
		
		sft_TestUtilities.createCustomSettings();
	}
	
	static testmethod void classConstructorTest() {
		
		sft_ConfigurationEditController cec = new sft_ConfigurationEditController();
		
		Integer appLength = Integer.valueOf(sft_Configuration__c.getAll().values()[0].get('Appointment_s_Length__c'));
		Integer reservPeriod = Integer.valueOf(sft_Configuration2__c.getInstance().TempReservationPeriod__c);
		String timeZone = String.valueOf(sft_Configuration__c.getAll().values()[0].get('Time_Zone__c'));
		
		System.assertEquals(appLength, 30);
		System.assertEquals(timeZone, cec.getTimeZone());	// tests getTimeZone() function
		System.assertEquals(reservPeriod, 5);

		List<SelectOption> selOptList = cec.getTimeZoneList();
		System.assertNotEquals(selOptList, null);
		
		cec.setTimeZone('Europe/Brussels');
		String tz = cec.timeZone;
		System.assertEquals(tz, 'Europe/Brussels');

	}
	
	static testmethod void saveTest() {
		
		sft_ConfigurationEditController cec = new sft_ConfigurationEditController();
        cec.appointmentLength = 25;
        cec.tempReservationPeriod = 6;
        cec.timeZone = 'Europe/Brussels';
        
        cec.save();
        
        Integer appLength = Integer.valueOf(sft_Configuration__c.getAll().values()[0].get('Appointment_s_Length__c'));
		Integer reservPeriod = Integer.valueOf(sft_Configuration2__c.getInstance().TempReservationPeriod__c);
		String timeZone = String.valueOf(sft_Configuration__c.getAll().values()[0].get('Time_Zone__c'));
		
		System.assertEquals(appLength, 25);
		System.assertEquals(timeZone, 'Europe/Brussels');
		System.assertEquals(reservPeriod, 6);
        
        	// let's try to enter incorrect values
        cec.appointmentLength = 4;
        cec.tempReservationPeriod = 4;
        
        cec.save();
        
        cec.cancel();
	}
    
    static testmethod void addNewWorkingHoursItemTest() {
        
        WorkingHours__c[] result;
        
        sft_ConfigurationEditController cec = new sft_ConfigurationEditController();
        cec.workingHoursItem.DayOfTheWeek__c = 1;
        cec.workingHoursItem.Name = '1:00';
        cec.workingHoursExceptionsItem.Name = '2:30';
        cec.workingHoursItem.DentistUser__c = UserInfo.getUserId();
        
        cec.addNewWorkingHoursItem();
        
        result = cec.getWorkingHoursList();
        
        System.assert(result != null);
        
        ApexPages.currentPage().getParameters().put('delWorkingHoursId', result[0].Id);
        cec.deleteWorkingHoursItem();
        
        result = cec.getWorkingHoursList();
        
        System.assert(result.size() == 0);
    }
	
    static testmethod void addNewWorkingHoursExceptionItem() {
        
        Date tomorrowDate = Date.today().addDays(1);
        Time tomorrowTime = Time.newInstance(2, 0, 0, 0);
        Datetime tomorrowDateTime = Datetime.newInstance(tomorrowDate, tomorrowTime);
        
        sft_ConfigurationEditController cec = new sft_ConfigurationEditController();
        cec.workingHoursExceptionsItem.StartTime__c = tomorrowDateTime;
        cec.workingHoursExceptionsItem.StopTime__c = tomorrowDateTime.addHours(2);
        
        cec.addNewWorkingHoursExceptionItem();
        
        WorkingHoursException__c[] result = cec.getWorkingHoursExceptionsList();
        
        System.assert(result != null);
    }
    
    static testmethod void getDayOfWeekListTest() {
        
        sft_ConfigurationEditController cec = new sft_ConfigurationEditController();
        
        SelectOption[] result = cec.getDayOfWeekList();
        
        System.assertEquals(result.size(), 7);
    }
}