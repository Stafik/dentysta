public with sharing class sft_DayOfWeekComponentController {
	
	public sft_AppointmentDatesWrapper adw { get; set; }
	public String result;
	
	public String getResult() {
		
		Integer dayOfWeekInt = sft_AppointmentReservationHelper.calculateNumericDayOfTheWeek(adw.getFirstDate());
		String result = sft_ConfigArray.integerToDayOfWeekMap.get(dayOfWeekInt) + ' (' + adw.getFirstDate().format() + ')';
		
		return result;
	}
    
}