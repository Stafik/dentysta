public class sft_DayOfWeekController {
	
	public Integer dayIntValue { get; set; }
	public String dayStringValue;
	
	public void setDayStringValue(String s) {
		this.dayStringValue = s;
	}
	
	public String getDayStringValue() {
		
		return sft_ConfigArray.integerToDayOfWeekMap.get(dayIntValue);
	}
}