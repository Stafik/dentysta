@RestResource(urlMapping='/Dentist/*')
global with sharing class sft_RESTService {
    
	/*
	 {
		"dentist" : "005580000018aIo",
		"contact" : "0035800000Guhxu",
		"year" : "2016",
		"month" : "10",
		"day" : "30",
		"hour" : "12",
		"minute" : "00",
		"type" : "Leczenie",
		"description" : "Opis..."
	}
	 */
		// this method assumes that the input data is valid and makes sense meaning that the appointment's date meets working hours and is not already booked
	@HttpPost
	global static Id createAppointment(String dentist, String contact, String year, String month, String day,
										String hour, String minute, String type, String description) {

		Appointment__c newAppointment;
		
			// let's check if none of the params is NULL
		if (dentist InstanceOf Id && contact InstanceOf Id && year != null && month != null && day != null &&
				hour != null && minute != null && type != null) {
			
				// let's try to construct a start DateTime
			Datetime startDateTime = constructAndValidateADateTime(year, month, day, hour, minute);
				// let's fetch a client as we need their name and surname later
			Contact[] client = [SELECT FirstName, LastName__c FROM Contact WHERE Id = :contact];
				// if startDateTime != null then it means we made it to construct a start date
			if (startDateTime != null) {
					// fetch Appointment's Length from the Configuration
				Integer appointmentDuration = Integer.valueOf(sft_Configuration__c.getAll().values()[0].get('Appointment_s_Length__c'));
					// construct an endDateTime (simply add appointment's length value)
				Datetime endDateTime = startDateTime.addMinutes(appointmentDuration);
				
				newAppointment = new Appointment__c();
					newAppointment.DentistUser__c = dentist;
					newAppointment.Contact__c = contact;
					newAppointment.StartDateAndTime__c = startDateTime;
					newAppointment.EndDateAndTime__c = endDateTime;
					newAppointment.AppointmentType__c = type;
					newAppointment.AppointmentStatus__c = 'Oczekuje na akceptację';
					newAppointment.Description__c = description;
					newAppointment.Name = sft_AppointmentReservationHelper.getAppointmentsName(newAppointment, client[0]);
					
				System.debug('newAppointment: ' + newAppointment);
				
				try {
					insert newAppointment;
				}
				catch (DmlException e) {
					System.debug('Couldn\'t insert a new appointment sent via REST POST. ' + e.getMessage());
				}
				
				sft_AppointmentReservationHelper.sendForApproval(newAppointment);
			}
		}

		return newAppointment.Id; 
	}
	
	
	@HttpGet
    global static List<String> getListOfDentists() {
		
		RestRequest request = RestContext.request;
		String optionsURL = request.requestURI.substring(request.requestURI.indexOf('/Dentist/') + 9);	// return the URL part that comes after '.../Dentist/'
		String[] optionsURLArray = optionsURL.split('/', 6);	// an array that keeps: { List | Id, [year], [month], [day] }
		String[] result = new List<String>();
		System.debug('optionsURLArray: ' + optionsURLArray);
		
			// /services/apexrest/Dentist/List - let's return list of Dentists
		if (optionsURLArray[0].toLowerCase() == 'list') {
			
			List<SelectOption> pickListOfDentists;
			List<String> listOfDentists = new List<String>();
			
				// let's use the controller method to fetch a list of dentists
			sft_AppointmentReservationController arc = new sft_AppointmentReservationController();
			pickListOfDentists = arc.getListOfDentists();
			
				// but the method returns a SelectOption (not a good idea though) list hence the convertion below:
			for (SelectOption dent : pickListOfDentists) {
				listOfDentists.add(dent.getLabel());
			}
			
			System.debug('@HTTPGET listOfDentists: ' + listOfDentists);
			
			result = listOfDentists;
		}
		
			// /services/apexrest/Dentist/a093000000VF09g - let's return list of available days for a particular dentist or available hours for a specific date, if given
		else if (optionsURLArray[0] InstanceOf ID) {
				
				// let's create an instance of the object as we will need to fetch Available Days or Available Slots
			sft_CalculateHours calcHrs = new sft_CalculateHours();
			
				// let's check if Date parameters are entered. If not let's display a list of Available Days for a given dentist 
			if (optionsURLArray.size() == 1) {
				
				List<String> availableDays = new List<String>();
				List<Decimal> availableDaysDecimal = new List<Decimal>();
				
					// let's use an existing method to get working hours for the particular dentist
				WorkingHours__c[] workingHrs = calcHrs.getStandardWorkingHours(optionsURLArray[0]);
				
					// let's add them to a temporary list (DayOfTheWeek__c field is a Decimal type)
				for (WorkingHours__c wh : workingHrs) {
					availableDaysDecimal.add(wh.DayOfTheWeek__c);
				}
					// sort it ascending
				availableDaysDecimal.sort();
				
					// create a final list that has day names
				for (Decimal avDayDec : availableDaysDecimal) {
					availableDays.add(sft_AppointmentReservationHelper.integerToDayOfWeekMap.get(avDayDec));
				}
				
				System.debug('availableDays: ' + availableDays);
				
				result = availableDays;
			}
				// it seems that there are some Date URL parameters so let's check them out and get some results
			else if (optionsURLArray.size() == 4) {
					// let's try to make use of the Date parameters and construct a date
				Date dateToCheckAvailability = constructAndValidateADate(optionsURLArray[1], optionsURLArray[2], optionsURLArray[3]);
					// we will keep here working slots for a particular dentist on a specific day
				List<sft_AppointmentDatesWrapper> availableSlots = new List<sft_AppointmentDatesWrapper>();
				
					// let's check if the parameters were correct and we made it to construct a new date
				if (dateToCheckAvailability != null) {
					
					availableSlots = calcHrs.getSlots(optionsURLArray[0], dateToCheckAvailability, dateToCheckAvailability);
					System.debug('availableSlots: ' + availableSlots);
					
					for (sft_AppointmentDatesWrapper adw : availableSlots) {
						for (sft_AppointmentDatesWrapper.InnerDate adwid : adw.listOfInnerDates) {
							result.add(adwid.startDateTime.format('YYYY-MM-dd HH:mm'));
						}
					}
				}
			}

		}
		
		return result;
	}
		// constucts a date out of 3 parameters given in the URI. If not possible (parameters are not correct) then it returns NULL
	private static Date constructAndValidateADate(String yearParam, String monthParam, String dayParam) {
		
		Integer year, month, day;
		Date result;
		
			// let's try to check if the parameters are really numbers?
		try {
			year = Integer.valueOf(yearParam);
			month = Integer.valueOf(monthParam);
			day = Integer.valueOf(dayParam);
		} catch (TypeException e) {
			System.debug('"Year", "Month" or "Day" URL parameter doesn\'t seem to be a number in the REST GET parameter in sft_DentistManager.cls. ' + e.getMessage());
			return null;
		}
			// params seem to be numbers so let's check if they make sense (not earlier than today and not a 999999999 year that causes an exception)
		if (year < Date.today().year() || year > 2100) {
			return null;
		}
			// let's try to costruct a date and let's check if 'month' and 'day' were not overstepped (e.g. 15 months = year + 1 and month + 3
		result = Date.newInstance(year, month, day);
		
		if (result.year() != year || result.day() != day || result.month() != month) {
			return null;
		}
		
		return result;
	}
	
		// performs a cursory validation and constructs a Datetime object or returns NULL
	private static DateTime constructAndValidateADateTime(String yearParam, String monthParam, String dayParam, String hourParam, String minuteParam) {
		
		Datetime result;
		Date dateChunk = constructAndValidateADate(yearParam, monthParam, dayParam);
		Integer hour = Integer.valueOf(hourParam);
		Integer minute = Integer.valueOf(minuteParam);
		
		if (hour < 23 && hour >= 0 && minute < 60 && minute >= 0) {
			if (dateChunk != null) {
				Time timeChunk = Time.newInstance(hour, minute, 0, 0);
				result = Datetime.newInstance(dateChunk, timeChunk);
			}
		}
		
		return result;
	}
     
}