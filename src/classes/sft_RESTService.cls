@RestResource(urlMapping='/Dentist/*')
global class sft_RESTService {
    
	/*
	 BASE: /services/apexrest/Dentist
	 								/List - lists all dentists
	 								/[Id] - lists available hours for a particular dentist
	 								/[Id]/[year]/[month]/[day] - lists available hours of a dentist for a particular day
	 {
		"dentist" : "005580000018aIo",
		"contactFirstName" : "Karol",
		"contactLastName" : "Łokieć",
		"contactPhone" : "5554725",
		"contactEmail" : "lokiec@example.com",
		"year" : "2016",
		"month" : "10",
		"day" : "30",
		"hour" : "12",
		"minute" : "00",
		"type" : "Leczenie",
		"description" : "Opis..."
	}
	 */
	JSONGenerator gen = JSON.createGenerator(true); // true - for pretty print formatting.
	
	//private static sft_RESTErrorResult err;
	private static sft_RESTResult err = new sft_RESTResult();
	
	global class sft_RESTResult {
		
		public String status;	// possible values: [OK] | [ERROR]
		public List<String> errors = new List<String>();
		public String newAppId;
		public List<DentistList> listOfDentists;
		public List<String> availableDays;
		public List<String> availableWorkingSlots;
	}
	
	public class DentistList {
		public Id dentId;
		public String firstName;
		public String lastName;
		public String dentistType;
		public String email;
	}
	
	private static final String usageHelp = 'Wrong syntax. Correct usage: /Dentist/List - fetches a list of dentists, ' +
			'/Dentist/[Id] - lists working days for the specified dentist, ' +
			'/Dentist/[Id]/[YYYY]/[MM]/[dd] - lists available hours for the specific dentist for a given date.';
	
	@HttpPost
	global static sft_RESTResult createAppointment(String dentist, String contactFirstName, String contactLastName, String contactPhone,
														String contactEmail, String year, String month, String day, String hour, String minute,
														String type, String description) {
		
		Appointment__c newAppointment;
		Contact newContact;
		Date datePart;
		Time timePart;
		Datetime startDateTime, stopDateTime;
		Integer appointmentDuration;
		Id result;
		
			// let's validate all parameters thoroughly
		validateNewAppParams(dentist, contactFirstName, contactLastName, contactPhone, contactEmail,
							year, month, day, hour, minute, type, description);
		System.debug('dentist = ' + dentist + ', contactFirstName = ' + contactFirstName + ', contactLastName = ' + contactLastName + 
					', contactPhone = ' + contactPhone + ', contactEmail = ' + contactEmail + ', year = ' + year + ', month = ' + month + ', day = ' + day +
					', hour = ' + hour + ', minute = ' + minute + ', type = ' + type + ', description = ' + description);
		System.debug('err = ' + err);
		
		
			// if there are no validation errors
		if (err.errors.size() == 0) {

				// let's try to construct a start DateTime
			datePart = Date.newInstance(Integer.valueOf(year), Integer.valueOf(month), Integer.valueOf(day));
			timePart = Time.newInstance(Integer.valueOf(hour), Integer.valueOf(minute), 0, 0);
			startDateTime = Datetime.newInstanceGMT(datePart, timePart);
				// fetch Appointment's Length from the Configuration
			appointmentDuration = Integer.valueOf(sft_Configuration__c.getAll().values()[0].get('Appointment_s_Length__c'));
				// construct an endDateTime (simply add appointment's length value)
			stopDateTime = startDateTime.addMinutes(appointmentDuration);
			
			// let's check if the date isn't already booked
			if (canTheDateBeBooked(dentist, startDateTime)) {
			
				// construct an Appointment
			newAppointment = new Appointment__c();
				newAppointment.DentistUser__c = dentist;
				newAppointment.StartDateAndTime__c = startDateTime;
				newAppointment.EndDateAndTime__c = stopDateTime;
				newAppointment.AppointmentType__c = type;
				newAppointment.AppointmentStatus__c = sft_ConfigArray.AWAITING_CONFIRMATION;
				newAppointment.Description__c = description;
				// construct a Contact
			newContact = new Contact(FirstName = contactFirstName, LastName = contactLastName, LastName__c = contactLastName, Phone = contactPhone, Email = contactEmail);
			System.debug('newAppointment = ' + newAppointment);
			System.debug('newContact = ' + newContact);
				// use Controller method to check if the contact already exists and insert a new Appointment
			result = sft_AppointmentReservationController.insertAppointment(newAppointment, newContact);
			
			err.status = 'OK';
			err.newAppId = result;
			
			} 
			else {
				err.status = 'ERROR';
				err.errors.add('The date and time entered isn\'t available. Please list available dates for the specific dentist and try once again.');
			}

		}
		else {
			// return errors added over validation and an ERROR status
			err.status = 'ERROR';
		}
	
		return err;
	}
	
	
	@HttpGet
    global static sft_RESTResult getListOfDentists() {
		
		RestRequest request = RestContext.request;
		String optionsURL = request.requestURI.substring(request.requestURI.indexOf('/Dentist/') + 9);	// return the URL part that comes after '.../Dentist/'
		String[] optionsURLArray = optionsURL.split('/', 6);	// an array that keeps: { List | Id, [year], [month], [day] }
		System.debug('optionsURLArray: ' + optionsURLArray);
		
			// /services/apexrest/Dentist/List - let's return list of Dentists
		if (optionsURLArray[0].toLowerCase() == 'list') {
			
			DentistList tempDent;
			
			err.listOfDentists = new List<DentistList>();
			err.status = 'OK';
			
			for (User usr : [SELECT Id, FirstName, LastName, DentistType__c, Email FROM User WHERE DentistType__c != null]) {
				tempDent = new DentistList();
				tempDent.dentId = usr.Id;
				tempDent.firstName = usr.FirstName;
				tempDent.lastName = usr.LastName;
				tempDent.dentistType = usr.DentistType__c;
				tempDent.email = usr.Email;
				
				err.listOfDentists.add(tempDent);
			}
			
		return err;
		}
		
			// /services/apexrest/Dentist/a093000000VF09g - let's return list of available days for a particular dentist or available hours for a specific date, if given
		else if (optionsURLArray[0] InstanceOf ID) {
				
				// let's create an instance of the object as we will need to fetch Available Days or Available Slots
			sft_CalculateHours calcHrs = new sft_CalculateHours();
			
				// let's check if Date parameters are entered. If not let's display a list of Available Days for a given dentist 
			if (optionsURLArray.size() == 1) {
				
				List<String> availableDaysString = new List<String>();
				List<Decimal> availableDaysDecimal = new List<Decimal>();
				
					// let's use an existing method to get working hours for the particular dentist
				WorkingHours__c[] workingHrs = calcHrs.getStandardWorkingHours(optionsURLArray[0]);
					
					System.debug('workingHrs: ' + workingHrs);
						// let's add them to a temporary List (DayOfTheWeek__c field is a Decimal type)
					for (WorkingHours__c wh : workingHrs) {
						availableDaysDecimal.add(wh.DayOfTheWeek__c);
					}
					
						// let's get rid of double elements coverting a list into a set
					Set<Decimal> tempSet = new Set<Decimal>(availableDaysDecimal);
						// once again into a list to sort it
					availableDaysDecimal = new List<Decimal>(tempSet);
						// sort it ascending
					availableDaysDecimal.sort();
					
						// create a final list that has day names
					for (Decimal avDayDec : availableDaysDecimal) {
						availableDaysString.add(sft_ConfigArray.integerToDayOfWeekMap.get(avDayDec));
					}
					
					System.debug('availableDays: ' + availableDaysString);
					
					err.status = 'OK';
					err.availableDays = availableDaysString;
					
					return err;

			}

				// it seems that there are some Date URL parameters so let's check them out and get some results
				// /apextest/Dentist/Id/YYYY/MM/DD
			else if (optionsURLArray.size() == 4) {
				
				Date dateToCheckAvailability;
					// let's try to make use of the Date parameters and check if the date is valid
				validateADate(optionsURLArray[1], optionsURLArray[2], optionsURLArray[3]);
				
				if (err.errors.size() == 0) {
					
					dateToCheckAvailability = Date.newInstance(Integer.valueOf(optionsURLArray[1]), Integer.valueOf(optionsURLArray[2]), Integer.valueOf(optionsURLArray[3]));
						// we will keep here working slots for a particular dentist on a specific day
					List<sft_AppointmentDatesWrapper> availableSlots = new List<sft_AppointmentDatesWrapper>();
					List<String> availableSlotsString = new List<String>();
						
					availableSlots = calcHrs.getSlots(optionsURLArray[0], dateToCheckAvailability, dateToCheckAvailability);
					System.debug('availableSlots: ' + availableSlots);
						
					for (sft_AppointmentDatesWrapper adw : availableSlots) {
						for (sft_AppointmentDatesWrapper.InnerDate adwid : adw.listOfInnerDates) {
							availableSlotsString.add(adwid.startDateTime.format('YYYY-MM-dd HH:mm'));
						}
					}
					
					err.status = 'OK';
					err.availableWorkingSlots = availableSlotsString;
				}
					// returns available slots or error list 
				return err;
			}
			else {
				err.status = 'ERROR';
				err.errors.add(usageHelp);
				
				return err;
			}
		}
		else {
			err.status = 'ERROR';
			err.errors.add(usageHelp);
			
			return err;
		}
	}
	
	private static void validateNewAppParams(String dentist, String contactFirstName, String contactLastName, String contactPhone,
														String contactEmail, String year, String month, String day, String hour, String minute,
														String type, String description) {
		
		String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$';
		Pattern emailPattern = Pattern.compile(emailRegex);
		
		if (dentist == null) {
			err.errors.add('\'dentist\' Id parameter value cannot be NULL');
		}
		else if (dentist == '') {
			err.errors.add('\'dentist\' Id parameter value cannot be empty');
		}
		else if (!(dentist InstanceOf Id)) {
			err.errors.add('\'dentist\' Id parameter value has to be an instance of User Id');
		}
		else if ([SELECT Id FROM User WHERE Id = :dentist].size() == 0) {
			err.errors.add('\'dentist\' Id parameter value doesn\'t point at any dentist (User)');
		}
		
		if (contactFirstName == null) {
			err.errors.add('\'contactFirstName\' parameter value cannot be NULL');
		}
		else if (contactFirstName == '') {
			err.errors.add('\'contacFirstName\' parameter value cannot be blank');
		}
		if (contactLastName == null) {
			err.errors.add('\'contactLastName\' parameter value cannot be NULL');
		}
		else if (contactLastName == '') {
			err.errors.add('\'contactLastName\' parameter value cannot be blank');
		}
		if (contactPhone == null) {
			err.errors.add('\'contactPhone\' parameter value cannot be NULL');
		}
		else if (contactPhone == '') {
			err.errors.add('\'contactPhone\' parameter value cannot be blank');
		}
		if (contactEmail == null) {
			err.errors.add('\'contactEmail\' parameter value cannot be NULL');
		}
		else if (contactEmail == '') {
			err.errors.add('\'contactEmail\' parameter value cannot be blank');
		}
		else if (!emailPattern.matcher(contactEmail).matches()) {
			err.errors.add('\'contactEmail\' parameter value doesn\'t seem to have a valid format');
		}
		
		validateADate(year, month, day);
		validateATime(hour, minute);
		
		if (type == NULL) {
			err.errors.add('\'type\' parameter value cannot be NULL');
		}
		else if (type == '') {
			err.errors.add('\'type\' parameter value cannot be blank');
		}
		else if (type.toLowerCase() != sft_ConfigArray.CHECKUP && type.toLowerCase() != sft_ConfigArray.TREATMENT) {
			err.errors.add('\'type\' parameter value can be set to \'check-up\' or \'treatment\' only');
		}
		else if (type.toLowerCase() == sft_ConfigArray.TREATMENT && (description == null || description == '')) {
			err.errors.add('\'description\' parameter value cannot be blank or NULL if \'type\' parameter is set to \'' + sft_ConfigArray.TREATMENT + '\'');
		}
	}
	
	// validates 3 date params
	private static void validateADate(String yearParam, String monthParam, String dayParam) {
		
		Integer year, month, day;
		Date result;
		
			// let's try to check if the parameters are really numbers?
		if (yearParam == null) {
			err.errors.add('\'year\' parameter value cannot be NULL');
		}
		else if (yearParam == '') {
			err.errors.add('\'year\' parameter value cannot be blank');
		}
		else {
			try {
				year = Integer.valueOf(yearParam);
				if (year < Date.today().year() || year > 2100) {
					err.errors.add('\'year\' parameter value should be between ' + Date.today().year() + ' and 2100');
				}
			} catch (TypeException e) {
				err.errors.add('\'year\' parameter value doesn\'t seem to be a number.');
			}
		}
		
		if (monthParam == null) {
			err.errors.add('\'month\' parameter value cannot be NULL');
		}
		else if (monthParam == '') {
			err.errors.add('\'month\' parameter value cannot be blank');
		}
		else {
			try {
				month = Integer.valueOf(monthParam);
				if (month < 1 || month > 12) {
					err.errors.add('\'month\' parameter value should be between 1 and 12');
				}
			} catch (TypeException e) {
				err.errors.add('\'month\' parameter value doesn\'t seem to be a number.');
			}
		}

		if (dayParam == null) {
			err.errors.add('\'day\' parameter value cannot be NULL');
		}
		else if (dayParam == '') {
			err.errors.add('\'day\' parameter value cannot be blank');
		}
		else {
			try {
				day = Integer.valueOf(dayParam);
				
				// it seems now that year and month make sense
				// let's try to costruct a date and let's check if 'day' value has not been overstepped (e.g. entering 32 days results in 1 additional month and some days)
				result = Date.newInstance(year, month, day);
			
				if (result.day() != day) {
					err.errors.add('\'day\' parameter value is not valid for this month and year.');
				}
			} catch (TypeException e) {
				err.errors.add('\'day\' parameter value doesn\'t seem to be a number.');
			}
		}
	}
	
	private static void validateATime(String hourParam, String minuteParam) {
		
		Integer hour, minute;
		
		if (hourParam == null) {
			err.errors.add('\'hour\' parameter value can\'t be NULL');
		}
		else if (hourParam == '') {
			err.errors.add('\'hour\' parameter value can\'t be empty');
		}
		else {
			try {
				hour = Integer.valueOf(hourParam);
				if (hour < 0 || hour > 23) {
					err.errors.add('\'hour\' parameter value should be between 0 and 23');
				}
			} catch (TypeException e) {
				err.errors.add('\'hour\' parameter value doesn\'t seem to be a number.');
			}
		}
		
		if (minuteParam == null) {
			err.errors.add('\'minute\' parameter value can\'t be NULL');
		}
		else if (minuteParam == '') {
			err.errors.add('\'minute\' parameter value can\'t be empty');
		}
		else {
			try {
				minute = Integer.valueOf(minuteParam);
				if (minute < 0 || minute > 59) {
					err.errors.add('\'minute\' parameter value should be between 0 and 23');
				}
			} catch (TypeException e) {
				err.errors.add('\'minute\' parameter value doesn\'t seem to be a number.');
			}
		}
	}
	
	private static Boolean canTheDateBeBooked(Id dentist, Datetime startDateTime) {
		
		Boolean result;
		
		sft_CalculateHours ch = new sft_CalculateHours();
		result = ch.canAppmntBeBooked(dentist, startDateTime);
		
		return result;
	}
     
}