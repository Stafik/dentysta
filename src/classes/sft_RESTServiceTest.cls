@isTest
public with sharing class sft_RESTServiceTest {
    
	private static User dentist;
	
	@testSetup static void setupTestData() {
		sft_TestUtilities.createUsers(3, 'test02611@02611.ru', 'Standard User', 'Stomatolog');
		sft_TestUtilities.createCustomSettings();
		System.assertEquals(sft_Configuration__c.getAll().values()[0].get('Appointment_s_Length__c'), 30);
    	System.assertEquals(sft_Configuration2__c.getInstance().TempReservationPeriod__c, 5);
    	
		sft_TestUtilities.createWorkingHours();
		sft_TestUtilities.createWorkingHoursExceptions();
		
	}
	
	static testmethod void createAppointmentTest() {
		
		dentist = [SELECT Id FROM User WHERE DentistType__c = 'Stomatolog' ORDER BY Id LIMIT 1];
		
		sft_TestUtilities.createContact();
		Contact cont = [SELECT Id FROM Contact LIMIT 1];
    	System.AssertNotEquals(cont, null);
		
		sft_RESTService.createAppointment(dentist.Id, 'Paweł', 'Stawinoga', '819384758', 'pstawinoga@example.com', '2016', '12', '23', '13', '30', 'Leczenie', 'Opis');
		Appointment__c app = [SELECT Id FROM Appointment__c WHERE DentistUser__c = :dentist.Id AND Contact__c = :cont.Id LIMIT 1];
    	System.AssertNotEquals(app, null);
    	
	}
	
	static testmethod void getListOfDentistsTest() {

		sft_RESTService.sft_RESTResult results;
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		
		req.requestURI = 'https://02611-dev-ed.my.salesforce.com/services/apexrest/Dentist/List';
		req.httpMethod = 'GET';
		RestContext.request = req;
		RestContext.response = res;
		
		results = sft_RESTService.getListOfDentists();
		System.Assert(results.status == 'OK');
		
			// list of available days test
		dentist = [SELECT Id FROM User WHERE DentistType__c = 'Stomatolog' ORDER BY Id LIMIT 1];
		
		req = new RestRequest();
		res = new RestResponse();
		req.requestURI = 'https://02611-dev-ed.my.salesforce.com/services/apexrest/Dentist/' + dentist.Id;
		req.httpMethod = 'GET';
		RestContext.request = req;
		RestContext.response = res;
		
		results = sft_RESTService.getListOfDentists();
		System.assert(results.listOfDentists.size() == 7);
		
					// list of available slots
		Date tomorrowDate = Date.today().addDays(1);
		req = new RestRequest();
		res = new RestResponse();
		req.requestURI = 'https://02611-dev-ed.my.salesforce.com/services/apexrest/Dentist/' + dentist.Id + '/' + tomorrowDate.year() + '/' + tomorrowDate.month() + 
			'/' + tomorrowDate.day();
		req.httpMethod = 'GET';
		RestContext.request = req;
		RestContext.response = res;
		
		results = sft_RESTService.getListOfDentists();
		System.assert(results.listOfDentists.size() != null && results.listOfDentists.size() > 0);
	}
	
	
}