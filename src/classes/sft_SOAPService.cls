global with sharing class sft_SOAPService {
	
	global static Response responsePayload = new Response();
	
	global class Response {
		webservice String status;
		webservice List<String> errors;
		webservice String newAppId;
		webservice List<DentistList> listOfDentists;
		webservice List<String> availableDays;
		webservice List<String> availableWorkingSlots;
		
		Response() {
			errors = new List<String>();
		}
	}
    
	global class DentistList {
		webservice Id dentId;
		webservice String firstName;
		webservice String lastName;
		webservice String dentistType;
		webservice String email;
	}
	
	webService static Response getListOfDentists() {
		
		DentistList dent;
		DentistList[] dList = new List<DentistList>();
		
		for (User usr : [SELECT Id, FirstName, LastName, DentistType__c, Email FROM User WHERE DentistType__c != null]) {
			
			dent = new DentistList();
			dent.dentId = usr.Id;
			dent.firstName = usr.FirstName;
			dent.lastName = usr.LastName;
			dent.dentistType = usr.DentistType__c;
			dent.email = usr.Email;
			
			dList.add(dent);
		}

		responsePayload.status = 'OK';
		responsePayload.listOfDentists = dList;
		System.debug('response = ' + responsePayload);
		
		return responsePayload;
	} 
	
	webService static Response getAvailableDays(Id dentist) {
		
		validateId(dentist);
		
		if (responsePayload.errors.size() == 0) {
			
			// let's create an instance of the object as we will need to fetch Available Days or Available Slots
			sft_CalculateHours calcHrs = new sft_CalculateHours();
			List<String> availableDays = new List<String>();
			List<Decimal> availableDaysDecimal = new List<Decimal>();
				
				// let's use an existing method to get working hours for the particular dentist
			WorkingHours__c[] workingHrs = calcHrs.getStandardWorkingHours(dentist);
				
				// let's add them to a temporary list (DayOfTheWeek__c field is a Decimal type)
			for (WorkingHours__c wh : workingHrs) {
				availableDaysDecimal.add(wh.DayOfTheWeek__c);
			}
				// sort it ascending
			availableDaysDecimal.sort();
				
				// create a final list that has day names
			for (Decimal avDayDec : availableDaysDecimal) {
				availableDays.add(sft_ConfigArray.integerToDayOfWeekMap.get(avDayDec));
			}
				
			System.debug('availableDays: ' + availableDays);
			
			responsePayload.status = 'OK';
			responsePayload.availableDays = availableDays;
		}
		else {
			// returns errors already saved in the 'responsePayload' static object
		}
		
		return responsePayload;
	}
	
	webService static Response getAvailableSlots(String dentist, Date dateVal) {
		
		validateId(dentist);
		validateDate(dateVal);
		
		if (responsePayload.errors.size() == 0) {
			
			sft_CalculateHours calcHrs = new sft_CalculateHours();
				// we will keep here working slots for a particular dentist on a specific day
			List<sft_AppointmentDatesWrapper> availableSlots = new List<sft_AppointmentDatesWrapper>();
			List<String> avSlotsList = new List<String>();
				
			availableSlots = calcHrs.getSlots(dentist, dateVal, dateVal);
			System.debug('availableSlots: ' + availableSlots);
				
			for (sft_AppointmentDatesWrapper adw : availableSlots) {
				for (sft_AppointmentDatesWrapper.InnerDate adwid : adw.listOfInnerDates) {
					avSlotsList.add(adwid.startDateTime.format('YYYY-MM-dd HH:mm'));
				}
			}
			
			responsePayload.status = 'OK';
			responsePayload.availableWorkingSlots = avSlotsList;
		}
		else {
			// there have been some validation errors so let's return them
		}
		
		return responsePayload;
	}
	
	webService static Id createAppointment(String dentist, String contact, Datetime startDateTime, String type, String description) {

		Appointment__c newAppointment;
		
		validateId(dentist);
		validateTypeAndDesc(type, description);
		
		
			// let's check if none of the params is NULL
		if (dentist InstanceOf Id && contact InstanceOf Id && startDateTime != null && type != null) {

				// let's fetch a client as we need their name and surname later
			Contact[] client = [SELECT FirstName, LastName__c FROM Contact WHERE Id = :contact];
				// fetch Appointment's Length from the Configuration
			Integer appointmentDuration = Integer.valueOf(sft_Configuration__c.getAll().values()[0].get('Appointment_s_Length__c'));
				// construct an endDateTime (simply add appointment's length value)
			Datetime endDateTime = startDateTime.addMinutes(appointmentDuration);
			
			newAppointment = new Appointment__c();
			
			newAppointment.DentistUser__c = dentist;
			newAppointment.Contact__c = contact;
			newAppointment.StartDateAndTime__c = startDateTime;
			newAppointment.EndDateAndTime__c = endDateTime;
			newAppointment.AppointmentType__c = type;
			newAppointment.AppointmentStatus__c = 'Oczekuje na akceptację';
			newAppointment.Description__c = description;
			newAppointment.Name = sft_AppointmentReservationHelper.getAppointmentsName(newAppointment, client[0]);
			
			System.debug('newAppointment: ' + newAppointment);
			
			try {
				insert newAppointment;
			}
			catch (DmlException e) {
				System.debug('Couldn\'t insert a new appointment sent via SOAP. ' + e.getMessage());
			}
			
			sft_AppointmentReservationHelper.sendForApproval(newAppointment);
		}
		
		return newAppointment.Id; 
	}
	
	private static void validateId(String idValue) {
		
		// let's do some validation and check if the Id given is really an Id?
		if (!(idValue InstanceOf Id)) {
			responsePayload.status = 'ERROR';
			responsePayload.errors.add('Given Id is not correct (probably a typo).');
		}
		// let's do some validation and check if the ID points at any dentist?
		if ([SELECT Id FROM User WHERE Id = :idValue].size() == 0) {
			responsePayload.status = 'ERROR';
			responsePayload.errors.add('It seems that the Id parameter doesn\'t point at any dentist (it isn\'t a User Id)');
		}
	}
	
	private static void validateDate(Date dateValue) {
		
		if (dateValue.year() > 2100 || dateValue.year() < Date.today().year()) {
			responsePayload.errors.add('Year cannot be lesser than ' + Date.today().year() + ' and greater than 2100.');
		}
	}
	
	private static void validateTypeAndDesc(String type, String description) {
		
		if (type != sft_ConfigArray.CHECKUP || type != sft_ConfigArray.TREATMENT) {
			responsePayload.errors.add('\'Type\' parameter has incorrect value. Possible values: ' + sft_ConfigArray.CHECKUP + ' or ' + sft_ConfigArray.TREATMENT);
			
		}
	}
}