@isTest
class sft_SOAPServiceTest {
	
	private static User dentist;
	
	@testSetup static void setupTestData() {
		sft_TestUtilities.createUsers(3, 'test02611@02611.ru', 'Standard User', 'Stomatolog');
		sft_TestUtilities.createCustomSettings();
		System.assertEquals(sft_Configuration__c.getAll().values()[0].get('Appointment_s_Length__c'), 30);
    	System.assertEquals(sft_Configuration2__c.getInstance().TempReservationPeriod__c, 5);
    	
		sft_TestUtilities.createWorkingHours();
		sft_TestUtilities.createWorkingHoursExceptions();
		
	}
	
    static testmethod void getListOfDentistsTest() {
    	
    	sft_SOAPService.Response response = sft_SOAPService.getListOfDentists();
    	System.debug('response = ' + response);
    	System.assert(response.listofDentists.size() >= 3);
    }
    
    static testmethod void getAvailableDaysTest() {
    	dentist = [SELECT Id FROM User WHERE DentistType__c = 'Stomatolog' ORDER BY Id LIMIT 1];
    	sft_SOAPService.Response response = sft_SOAPService.getAvailableDays(dentist.Id);
    	System.assertEquals(response.availableDays.size(), 7);
    }
    
    static testmethod void getAvailableHoursTest() {
    	
    	dentist = [SELECT Id FROM User WHERE DentistType__c = 'Stomatolog' ORDER BY Id LIMIT 1];
    	sft_SOAPService.Response response = sft_SOAPService.getAvailableSlots(dentist.Id, Date.today().addDays(1));
    	System.assertNotEquals(response.availableWorkingSlots.size(), 0);
    }
    
    static testmethod void createAppointmentTest() {
    	
    	Contact cont;
    	sft_CalculateHours calcHrs;
    	List<sft_AppointmentDatesWrapper> slotList;
    	Datetime startDateTime;
    	
    	dentist = [SELECT Id FROM User WHERE DentistType__c = 'Stomatolog' ORDER BY Id LIMIT 1];
    	
    	sft_TestUtilities.createContact();
    	cont = [SELECT Id FROM Contact LIMIT 1];
    	System.AssertNotEquals(cont, null);
    	
    		// let's fetch the first free slot for a given dentist
    	calcHrs = new sft_CalculateHours();
    	slotList = calcHrs.getSlots(dentist.Id, Date.today(), Date.today());
    	startDateTime = slotList[0].listOfInnerDates[0].startDateTime;
    	
    		// let's create a new Appointment eventually
    	sft_SOAPService.createAppointment(dentist.Id, cont.Id, startDateTime, 'Leczenie', 'Opis');
    	Appointment__c app = [SELECT Id FROM Appointment__c WHERE DentistUser__c = :dentist.Id AND Contact__c = :cont.Id LIMIT 1];
    	System.AssertNotEquals(app, null);
    }
}