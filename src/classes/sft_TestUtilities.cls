public class sft_TestUtilities {
	
	public static User createUser(String firstName, String lastName, String email, String profile, String dentistType) {
		
		Profile profileId = [SELECT Id FROM Profile WHERE Name = :profile LIMIT 1];
		
		User usr = new User(LastName = lastName,
							FirstName = firstName,
							Alias = lastName.substring(0, 3) + firstName.substring(0, 1),
							Email = email,
							Username = email,
							ProfileId = profileId.Id,
							TimeZoneSidKey = 'GMT',
							LanguageLocaleKey = 'en_US',
							EmailEncodingKey = 'UTF-8',
							LocaleSidKey = 'pl_PL',
							DentistType__c = dentistType
							);
		insert usr;
		
		return usr;
	}
	
	public static User[] createUsers(Integer quantity, String email, String profile, String dentistType) {
		
		User[] users = new List<User>();
		
		for (Integer i = 0; i < quantity; i++) {
			users.add(createUser('UserName' + i, 'UserSurname' + i, email + i , profile, dentistType));
		}
		System.debug('users: ' + users);
		
		return users;
	}
	
	public static void createCustomSettings() {
		
			// let's recall default Custom Settings
		insert new sft_Configuration__c(Name = 'sft_Configuration', Appointment_s_Length__c = 30, Time_Zone__c = 'Europe/Berlin');
		insert new sft_Configuration2__c(Name = 'sft_Configuration2', TempReservationPeriod__c = 5);
	}
	
	public static void createWorkingHours() {
		
		WorkingHours__c wh;
		WorkingHours__c[] whList = new List<WorkingHours__c>();
		
		for (Integer i = 1; i <= 7; i++) {
			wh = new WorkingHours__c();
			wh.DayOfTheWeek__c = i;
			wh.DentistUser__c = [SELECT Id FROM User WHERE DentistType__c = 'Stomatolog' ORDER BY Id LIMIT 1].Id;
			wh.StartTime__c = 600 + i * 5;
			wh.StopTime__c = 900 + i * 5;
			whList.add(wh);
			
			wh = new WorkingHours__c();
			wh.DayOfTheWeek__c = i;
			wh.DentistUser__c = UserInfo.getUserId();
			wh.StartTime__c = 60;
			wh.StopTime__c = 120;
			whList.add(wh);
		}
		
		insert whList;
	}
	
	public static void createWorkingHoursWithId(Id dentist) {
		
		WorkingHours__c wh;
		WorkingHours__c[] whList = new List<WorkingHours__c>();
		
		for (Integer i = 1; i <= 7; i++) {
			wh = new WorkingHours__c();
			wh.DayOfTheWeek__c = i;
			wh.DentistUser__c = dentist;
			wh.StartTime__c = 600 + i * 5;
			wh.StopTime__c = 900 + i * 5;
			whList.add(wh);
			
			wh = new WorkingHours__c();
			wh.DayOfTheWeek__c = i;
			wh.DentistUser__c = UserInfo.getUserId();
			wh.StartTime__c = 60;
			wh.StopTime__c = 120;
			whList.add(wh);
		}
		
		insert whList;
	}
	
	
	public static void createWorkingHoursExceptions() {
		
		WorkingHoursException__c whex;
		WorkingHoursException__c[] whexList = new List<WorkingHoursException__c>();
		Time timeChunk = Time.newInstance(10, 35, 00, 00);
		
		for (Date d = Date.today(); d <= Date.today().addDays(7); d = d.addDays(1)) {

			whex = new WorkingHoursException__c(); 
			whex.StartTime__c = Datetime.newInstance(d, timeChunk);
			whex.StopTime__c = Datetime.newInstance(d, timeChunk).addMinutes(30);
			timeChunk = timeChunk.addMinutes(30);
			
			whexList.add(whex);
			
			timeChunk.addMinutes(30); // let's shift it 30 min
			whex = new WorkingHoursException__c(); 
			whex.StartTime__c = Datetime.newInstance(d, timeChunk);
			whex.StopTime__c = Datetime.newInstance(d, timeChunk).addMinutes(30);
			
			whexList.add(whex);
		}
		
		insert whexList;
	}
	
	public static void createWorkingHoursExceptionsWithId(Id dentist) {
		
		WorkingHoursException__c whex;
		WorkingHoursException__c[] whexList = new List<WorkingHoursException__c>();
		Time timeChunk = Time.newInstance(10, 35, 00, 00);
		
		for (Date d = Date.today(); d <= Date.today().addDays(7); d = d.addDays(1)) {

			whex = new WorkingHoursException__c(); 
			whex.DentistUser__c = dentist;
			whex.StartTime__c = Datetime.newInstance(d, timeChunk);
			whex.StopTime__c = Datetime.newInstance(d, timeChunk).addMinutes(30);
			timeChunk = timeChunk.addMinutes(30);
			
			whexList.add(whex);
			
			timeChunk.addMinutes(30); // let's shift it 30 min
			whex = new WorkingHoursException__c(); 
			whex.StartTime__c = Datetime.newInstance(d, timeChunk);
			whex.StopTime__c = Datetime.newInstance(d, timeChunk).addMinutes(30);
			
			whexList.add(whex);
		}
		
		insert whexList;
	}
	
	public static void createContact() {
		
		Contact cont = new Contact(LastName = 'LastName', LastName__c = 'LastName', FirstName = 'FirstName', Email = 'email@example.com', Phone = '77740349');
		insert cont;
	}
}