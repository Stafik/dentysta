@isTest
class sft_TimeConverterControllerTest {

	static testmethod void timeConverterTest() {
		
		sft_TimeConverterController tc = new sft_TimeConverterController();
		
		tc.setHours(2);
		System.assertEquals(tc.hours, 2);
		
		tc.timeValue = 615;
		Integer h = tc.getHours();
		System.assertEquals(h, 10);
		
		tc.setMinutes(10);
		System.assertEquals(tc.minutes, 10);
		
		Integer m = tc.getMinutes();
		System.assertEquals(m, 15);
	}    
}