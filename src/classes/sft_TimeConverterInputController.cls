public class sft_TimeConverterInputController {
    public String timeVal { get; set; }
    public Integer tempStartTime1;
    
    public void setTempStartTime1(Integer i) {
    	this.tempStartTime1 = i;
    }
    
    public Integer getTempStartTime1() {
    	String[] hourAndMinutes = timeVal.split(':');
		Integer HH = Integer.valueOf(hourAndMinutes[0]);
		Integer mm = Integer.valueOf(hourAndMinutes[1]);
		
		Integer result = 60 * HH + mm;
		System.debug('convertTimeToInt result: ' + result);
		
		return result;
    }
}