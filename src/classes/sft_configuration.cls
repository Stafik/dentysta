public with sharing class sft_configuration {
	
	private List<sft_Configuration__c> config;
	public Integer appointmentLength { get; set; }
	public Integer tempReservationPeriod { get; set; }
	public WorkingHours__c workingHoursList;
	public WorkingHoursException__c workingHoursExceptionsList;
	//public String timeZone { get; set; }
	
	public sft_configuration() {
		config = sft_Configuration__c.getAll().values();
		System.assert(config.size() == 1);
		appointmentLength = Integer.valueOf(config[0].get('Appointment_s_Length__c'));
		
			// fetch TempReservationPeriod from Hierarchy Custom Settings
		Decimal decimal_tempReservationPeriod = sft_Configuration2__c.getInstance().TempReservationPeriod__c;
		tempReservationPeriod = Integer.valueOf(decimal_tempReservationPeriod);
		
		workingHoursList = new WorkingHours__c();
		WorkingHoursExceptionsList = new WorkingHoursException__c();
	}
	
	public String getTimeZone() {
		
		String result;
		
		String tzone = String.valueOf(config[0].get('Time_Zone__c'));
		Timezone tz = Timezone.getTimeZone(tzone);
		String tzDisplayName = tz.getDisplayName();
		
		Schema.DescribeFieldResult fieldResult = User.TimeZoneSidKey.getdescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for(Schema.PicklistEntry p : ple)
		{
			if (p.getValue().equals(tzone)) {
				result = p.getlabel().substring(0, 11);
			}
		}
		result = result + ' ' + tzDisplayName + ' (' + tzone + ')';
		
		return result;
	}
	
	public PageReference edit() {
		
		return Page.sft_ConfigurationEdit;
	}
	
	
	public WorkingHours__c[] getWorkingHoursList() {
		
		WorkingHours__c[] whList;
		Id userId = UserInfo.getUserId();
		
		whList = [SELECT DayOfTheWeek__c, DentistUser__c, StartTime__c, StopTime__c
		          FROM WorkingHours__c
		          WHERE DentistUser__c = :userId
		          ORDER BY DayOfTheWeek__c, StartTime__c];
		
		return whList;
	}

	public WorkingHoursException__c[] getWorkingHoursExceptionsList() {
		
		WorkingHoursException__c[] whexList;
		Id userId = UserInfo.getUserId();
		
		whexList = [SELECT DentistUser__c, StartTime__c, StopTime__c
		          FROM WorkingHoursException__c
		          WHERE DentistUser__c = :userId
		          ORDER BY StartTime__c];
		
		return whexList;
	}
}