public class xxx {
	@InvocableMethod
	public static void deleteTempReservation(Id[] tempReservationId) {
		
		try {
			Database.delete(tempReservationId);
		}
		catch (DmlException e) {
			System.debug('Can\'t delete a temporary reservation in class "xxx": ' + e.getMessage());
		}
	}
	
}