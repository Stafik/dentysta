/*** When a new reservation is being made it's inserted into SFDC and marked as a temporary (TemporaryReservation__c = true)
	 A time dependent workflow ("Unblock reservation after 5 min") comes in 5 minutes since the record creation date and changes
	 the TemporaryReservation__c field to false.
	 This triggers the following snippet which deletes the record. 
***/

trigger sft_cleanTempReservationUp on Appointment__c (after update) {
	/*
		// a temporary Appointment has no Contact defined so it can be deleted as soon as the time trigger unsets the 'temporary reservation' flag
    for (Appointment__c app : Trigger.new) {
    	if (app.Contact__c == null) {
    		Appointment__c appToDelete = new Appointment__c();
    		appToDelete.Id = app.Id;
    		delete appToDelete;
    		System.debug('delete w triggerze: ' + appToDelete);
    	}
    }
    */
}